# **Monitor**

# **Running the project**

First, you need to have [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/install/)
installed in your machine.

Then, you should run the script ``seed.sh`` in the *server* folder (see [Seed](server/README.md#Seed)).

After that, you can execute the script ``run.sh`` to start the application.\
This script will create a container with the web client and a container with the web server.

The web server will be running on http://0.0.0.0:4000 and the web client, on http://0.0.0.0:3000.

## **Testing**

- [Web project](web/README.md)
- [Server project](server/README.md)

## **Phase 1**

- Web project created
- Server project created
- Requirements 1 and 2 implemented in web project with unit tests
- Created generic components in frontend to avoid code duplication
- Created models interfaces in backend that will be used in next phases

## **Phase 2**

- Added autocomplete for selecting parties and deputies
- Requirement 3: inclusion of keywords
- Requirement 4: load votes resume from the selected parties/deputies
- Created seed script that fetch data from [Dados Abertos v1](https://www2.camara.leg.br/transparencia/dados-abertos/dados-abertos-legislativo)
and [Dados Abertos v2](https://dadosabertos.camara.leg.br/swagger/api.html), and save them
locally (see [Seed](server/README.md#Seed))

## **Phase 3**

- **Must do the [seed step](server/README.md#Seed) again before running the project**
- Requirement 5: monitored entities votes listing with period and keyword filters
- Added seed for propositions details
- Added api route for fetching deputies and parties votes
- Added integration tests