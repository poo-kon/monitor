import Entity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import VotesResumeEntity from 'domain/votes/entities/votes_resume_entity';
import { createContext } from 'react';

type VotesResumesContextType = {
  votesResumes: VotesResumeEntity[];
  loadVotesResumes: (entities: Entity[], period: PeriodEntity) => void;
  retry: () => void;

  loading: boolean;
  failed: boolean;
  done: boolean;
};
const VotesResumesContext = createContext<VotesResumesContextType>({
  votesResumes: [],
  loadVotesResumes: () => {},
  retry: () => {},

  loading: false,
  failed: false,
  done: false,
});

export default VotesResumesContext;
