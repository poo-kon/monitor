import { useContext } from 'react';
import VotesResumesContext from './VotesResumesContext';

const useVotesResumes = () => {
  return useContext(VotesResumesContext);
};

export default useVotesResumes;
