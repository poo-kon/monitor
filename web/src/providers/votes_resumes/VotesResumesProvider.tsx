import Entity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import VotesResumeEntity from 'domain/votes/entities/votes_resume_entity';
import loadVotesResumesUseCase from 'domain/votes/usecases/load_votes_resumes_usecase';
import { FC, useState } from 'react';
import useRequest from 'utils/useRequest';
import VotesResumesContext from './VotesResumesContext';

type VotesResumesRequest = {
  period: PeriodEntity;
  entities: Entity[];
};
const VotesResumesProvider: FC = ({ children }) => {
  const [votesResumes, setVotesResumes] = useState<VotesResumeEntity[]>([]);
  const [votesResumesRequest, setVotesResumesRequest] =
    useState<VotesResumesRequest>({
      entities: [],
      period: {
        start: null,
        end: null,
      },
    });

  const { done, failed, loading, submit } = useRequest(
    () => {
      const { entities, period } = votesResumesRequest;
      return loadVotesResumesUseCase(entities, period);
    },
    (votesResumes) => setVotesResumes(votesResumes)
  );

  const loadVotesResumes = (entities: Entity[], period: PeriodEntity) => {
    if (
      entities === votesResumesRequest.entities &&
      period === votesResumesRequest.period
    )
      return;
    setVotesResumesRequest({
      period,
      entities,
    });
    submit();
  };

  const retry = () => {
    if (!failed) return;
    submit();
  };

  return (
    <VotesResumesContext.Provider
      value={{ votesResumes, loadVotesResumes, retry, loading, failed, done }}
    >
      {children}
    </VotesResumesContext.Provider>
  );
};

export default VotesResumesProvider;
