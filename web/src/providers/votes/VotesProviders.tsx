import ActorEntity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import ActorsPropositionsVotingsEntity from 'domain/votes/entities/actors_propositions_votings_entity';
import loadActorsVotesUseCase from 'domain/votes/usecases/load_actors_votes_usecase';
import { FC, useState } from 'react';
import useRequest from 'utils/useRequest';
import VotesContext from './VotesContext';

type VotesRequest = {
  actors: ActorEntity[];
  period: PeriodEntity;
};
const VotesProvider: FC = ({ children }) => {
  const [votesRequest, setVotesRequest] = useState<VotesRequest>({
    actors: [],
    period: { start: null, end: null },
  });
  const [actorsPropositionsVotings, setActorsPropositionsVotings] =
    useState<ActorsPropositionsVotingsEntity>({});

  const { done, failed, loading, submit } = useRequest(
    () => {
      const { actors, period } = votesRequest;
      return loadActorsVotesUseCase(actors, period);
    },
    (actorsPropositionsVotings) =>
      setActorsPropositionsVotings(actorsPropositionsVotings)
  );

  const loadVotes = (actors: ActorEntity[], period: PeriodEntity) => {
    if (actors === votesRequest.actors && period === votesRequest.period)
      return;

    setVotesRequest({
      actors,
      period,
    });
    submit();
  };

  const retry = () => {
    if (!failed) return;
    submit();
  };

  return (
    <VotesContext.Provider
      value={{
        loadVotes,
        actorsPropositionsVotings,
        retry,
        done,
        failed,
        loading,
      }}
    >
      {children}
    </VotesContext.Provider>
  );
};

export default VotesProvider;
