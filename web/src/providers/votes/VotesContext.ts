import ActorEntity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import ActorsPropositionsVotingsEntity from 'domain/votes/entities/actors_propositions_votings_entity';
import { createContext } from 'react';

type VotesContextType = {
  actorsPropositionsVotings: ActorsPropositionsVotingsEntity;
  loadVotes: (actors: ActorEntity[], period: PeriodEntity) => void;
  retry: () => void;

  loading: boolean;
  failed: boolean;
  done: boolean;
};
const VotesContext = createContext<VotesContextType>({
  actorsPropositionsVotings: {},
  loadVotes: () => {},
  retry: () => {},

  loading: false,
  failed: false,
  done: false,
});

export default VotesContext;
