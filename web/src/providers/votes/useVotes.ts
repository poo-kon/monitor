import { useContext } from 'react';
import VotesContext from './VotesContext';

const useVotes = () => {
  return useContext(VotesContext);
};

export default useVotes;
