import loadPartiesUseCase from 'domain/entities/load_parties_usecase';
import EntitiesProvider from 'providers/entities/EntitiesProvider';
import { FC } from 'react';
import PartyContext from './PartiesContext';

const PartiesProvider: FC = (props) => (
  <EntitiesProvider
    {...props}
    context={PartyContext}
    loadEntitiesUseCase={loadPartiesUseCase}
  />
);

export default PartiesProvider;
