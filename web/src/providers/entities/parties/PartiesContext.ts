import { createContextEntity } from 'providers/entities/EntitiesContext';

const PartiesContext = createContextEntity();

export default PartiesContext;
