import { useContext } from 'react';
import PartiesContext from './PartiesContext';

const useParties = () => {
  const { entities, failed, loadEntities, loading } =
    useContext(PartiesContext);

  return {
    parties: entities,
    loadParties: loadEntities,
    loading,
    failed,
  };
};

export default useParties;
