import { createContextEntity } from 'providers/entities/EntitiesContext';

const DeputiesContext = createContextEntity();

export default DeputiesContext;
