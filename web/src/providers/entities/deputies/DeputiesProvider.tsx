import loadDeputiesUseCase from 'domain/entities/load_deputies_usecase';
import EntitiesProvider from 'providers/entities/EntitiesProvider';
import { FC } from 'react';
import DeputiesContext from './DeputiesContext';

const DeputiesProvider: FC = (props) => (
  <EntitiesProvider
    {...props}
    context={DeputiesContext}
    loadEntitiesUseCase={loadDeputiesUseCase}
  />
);

export default DeputiesProvider;
