import DeputiesContext from 'providers/entities/deputies/DeputiesContext';
import { useContext } from 'react';

const useDeputies = () => {
  const { failed, entities, loadEntities, loading } =
    useContext(DeputiesContext);

  return {
    deputies: entities,
    loadDeputies: loadEntities,
    loading,
    failed,
  };
};

export default useDeputies;
