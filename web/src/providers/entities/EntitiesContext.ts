import Entity from 'domain/entities/entity';
import { createContext } from 'react';

export type EntitiesContextType = {
  loading: boolean;
  failed: boolean;
  loadEntities: () => void;
  entities: Array<Entity>;
};

export const createContextEntity = () =>
  createContext<EntitiesContextType>({
    loading: false,
    failed: false,
    loadEntities: () => {},
    entities: [],
  });
