import Entity from 'domain/entities/entity';
import { Context, FC, useCallback, useEffect, useState } from 'react';
import useRequest from 'utils/useRequest';
import { EntitiesContextType } from './EntitiesContext';

type Props = {
  context: Context<EntitiesContextType>;
  loadEntitiesUseCase: () => Promise<Array<Entity>>;
};
const EntitiesProvider: FC<Props> = ({
  context,
  loadEntitiesUseCase,
  children,
}) => {
  const [init, setInit] = useState(false);
  const [entities, setEntities] = useState<Array<Entity>>([]);
  const [failed, setFailed] = useState(false);
  const { loading, submit, done } = useRequest(
    async () => {
      try {
        const entities = await loadEntitiesUseCase();
        setFailed(false);
        return entities;
      } catch (error) {
        setFailed(true);
        throw error;
      }
    },
    (entities) => setEntities(entities),
    800
  );

  const loadEntities = useCallback(() => {
    if (!done) submit();
  }, [done, submit]);

  useEffect(() => {
    if (init) return;
    setInit(true);
    loadEntities();
  }, [loadEntities, init]);

  return (
    <context.Provider value={{ loading, failed, loadEntities, entities }}>
      {children}
    </context.Provider>
  );
};

export default EntitiesProvider;
