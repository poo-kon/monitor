import DeputyEntity from 'domain/entities/deputy_entity';
import deputiesService from 'services/deputies';

const loadDeputiesUseCase = async (): Promise<Array<DeputyEntity>> => {
  return deputiesService.fetchDeputies();
};

export default loadDeputiesUseCase;
