import Entity from './entity';

class PartyEntity extends Entity {
  initials: string;

  constructor(id: string, name: string, initials: string) {
    super(id, name);
    this.initials = initials;
  }
}

export default PartyEntity;
