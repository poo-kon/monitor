import PartyEntity from 'domain/entities/party_entity';
import partiesService from 'services/parties';

const loadPartiesUseCase = async (): Promise<Array<PartyEntity>> => {
  return partiesService.fetchParties();
};

export default loadPartiesUseCase;
