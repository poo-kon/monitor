import ActorEntity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import ActorsPropositionsVotingsEntity from 'domain/votes/entities/actors_propositions_votings_entity';
import votesService from 'services/votes';

const loadActorsVotesUseCase = async (
  actors: ActorEntity[],
  period: PeriodEntity
): Promise<ActorsPropositionsVotingsEntity> => {
  const actorsPropositionsVotings = await Promise.all(
    actors.map(async (actor) => votesService.fetchActorVotes(actor, period))
  );

  return actorsPropositionsVotings.reduce(
    (response, actorPropositionsVotings) => ({
      ...response,
      [actorPropositionsVotings.actor.id]: actorPropositionsVotings,
    }),
    {}
  );
};

export default loadActorsVotesUseCase;
