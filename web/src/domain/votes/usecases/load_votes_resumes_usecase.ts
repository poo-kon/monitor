import Entity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import VotesResumeEntity from 'domain/votes/entities/votes_resume_entity';
import votesService from 'services/votes';

const loadVotesResumesUseCase = async (
  entities: Entity[],
  period: PeriodEntity
): Promise<VotesResumeEntity[]> => {
  return await votesService.fetchVotesResume(entities, period);
};

export default loadVotesResumesUseCase;
