class VoteEntity {
  value: string;

  constructor(vote: string = '') {
    this.value = vote.toUpperCase();

    if (this.value === '') this.value = 'AUSENTE';
  }
}

export default VoteEntity;
