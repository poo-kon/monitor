type VotingEntity = {
  resume: string;
  objective: string;
  date: Date;
};

export default VotingEntity;
