type PropositionEntity = {
  code: number;

  type: string;
  number: number;
  year: number;

  theme: string;
  description: string;
};

export default PropositionEntity;
