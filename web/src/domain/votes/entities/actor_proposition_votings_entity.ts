import ActorEntity from 'domain/entities/entity';
import PropositionEntity from './proposition_entity';
import VoteEntity from './vote_entity';
import VotingEntity from './voting_entity';

type ActorPropositionsVotingsEntity = {
  actor: ActorEntity;
  propositionsVotings: {
    proposition: PropositionEntity;
    actorVotings: {
      voting: VotingEntity;
      vote: VoteEntity;
    }[];
  }[];
};

export default ActorPropositionsVotingsEntity;
