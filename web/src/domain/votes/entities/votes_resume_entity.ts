import Entity from 'domain/entities/entity';
import PartyEntity from 'domain/entities/party_entity';

class VotesResumeEntity {
  yes?: number;
  no?: number;
  obstruct?: number;
  free?: number;
  missing?: number;
  entity: Entity;

  constructor(
    entity: Entity,
    params: {
      yes?: number;
      no?: number;
      obstruct?: number;
      free?: number;
      missing?: number;
    } = {}
  ) {
    const { yes, no, obstruct, free, missing } = params;

    this.entity = entity;
    this.yes = yes;
    this.no = no;
    this.obstruct = obstruct;
    this.missing = missing;

    if (this.entity instanceof PartyEntity) this.free = free;
  }
}

export default VotesResumeEntity;
