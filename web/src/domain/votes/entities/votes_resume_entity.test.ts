import DeputyEntity from 'domain/entities/deputy_entity';
import PartyEntity from 'domain/entities/party_entity';
import each from 'jest-each';
import VotesResumeEntity from './votes_resume_entity';

describe('VotesResumeEntity', () => {
  const mockDeputy = new DeputyEntity('id', 'name');
  const mockParty = new PartyEntity('id', 'name', 'initials');

  it('should construct votes resume entity with default values', () => {
    const votesResume = new VotesResumeEntity(mockParty, {
      free: 4,
      yes: 10,
    });

    expect(votesResume.entity).toBe(mockParty);
    expect(votesResume.free).toBe(4);
    expect(votesResume.yes).toBe(10);
    expect(votesResume.no).toBe(undefined);
    expect(votesResume.missing).toBe(undefined);
    expect(votesResume.obstruct).toBe(undefined);
  });

  each([
    [
      new VotesResumeEntity(mockDeputy, { yes: 5, free: 0, missing: 3 }),
      { yes: 5, missing: 3 },
    ],
    [new VotesResumeEntity(mockDeputy, { free: 10, no: 5 }), { no: 5 }],
    [
      new VotesResumeEntity(mockDeputy, { yes: 15, no: 5, missing: 20 }),
      { yes: 15, no: 5, missing: 20 },
    ],
  ]).it(
    'should construct votes resume entity for deputy votes resume',
    (
      votesResume: VotesResumeEntity,
      expectedVotesCount: { [key: string]: number | undefined }
    ) => {
      expect(votesResume.entity).toBe(mockDeputy);
      expect(votesResume.yes).toBe(expectedVotesCount.yes);
      expect(votesResume.no).toBe(expectedVotesCount.no);
      expect(votesResume.obstruct).toBe(expectedVotesCount.obstruct);
      expect(votesResume.missing).toBe(expectedVotesCount.missing);
      expect(votesResume.free).toBe(expectedVotesCount.free);
    }
  );
});
