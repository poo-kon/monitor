import ActorPropositionsVotingsEntity from './actor_proposition_votings_entity';

type ActorsPropositionsVotingsEntity = {
  [key: string]: ActorPropositionsVotingsEntity;
};

export default ActorsPropositionsVotingsEntity;
