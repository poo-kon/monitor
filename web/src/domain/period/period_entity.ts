type PeriodEntity = {
  start: Date | null;
  end: Date | null;
};

export default PeriodEntity;
