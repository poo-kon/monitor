import PeriodEntity from './period_entity';

export class EmptyOrInvalidDateError extends Error {}
export class GreaterThanEndError extends Error {}

type PeriodErrorsKey = 'start' | 'end';
type PeriodErrors = {
  [key in PeriodErrorsKey]?: Error;
};

const validatePeriodUseCase = (period: PeriodEntity): PeriodErrors | null => {
  const errors: PeriodErrors = {};

  if (!period.start) errors['start'] = new EmptyOrInvalidDateError();
  if (!period.end) errors['end'] = new EmptyOrInvalidDateError();

  if (period.start && period.end && period.start > period.end)
    errors['start'] = new GreaterThanEndError();

  return !Object.keys(errors).length ? null : errors;
};

export default validatePeriodUseCase;
