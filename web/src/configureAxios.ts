import axios from 'axios';
import { API_URL } from 'config/api_url';

const configureAxios = () => {
  axios.defaults.baseURL = API_URL;
};

export default configureAxios;
