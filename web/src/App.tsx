import { Grid, Typography } from '@mui/material';
import MonitorComponent from 'components/monitor/MonitorComponent';
import APP_NAME from 'config/app_name';
import configureAxios from 'configureAxios';
import DeputiesProvider from 'providers/entities/deputies/DeputiesProvider';
import PartiesProvider from 'providers/entities/parties/PartiesProvider';
import VotesProvider from 'providers/votes/VotesProviders';
import VotesResumesProvider from 'providers/votes_resumes/VotesResumesProvider';
import { useEffect } from 'react';

const App = () => {
  useEffect(() => {
    configureAxios();
  }, []);

  return (
    <VotesProvider>
      <VotesResumesProvider>
        <PartiesProvider>
          <DeputiesProvider>
            <Grid
              container
              justifyContent="center"
              sx={{ padding: 1, marginTop: 8 }}
            >
              <Grid item container xs={12} sm={10} md={8} lg={7}>
                <Grid item container justifyContent="center">
                  <Typography variant="h3" textAlign="center">
                    <b>{APP_NAME}</b>
                  </Typography>
                </Grid>

                <Grid item container sx={{ marginTop: 8 }}>
                  <MonitorComponent />
                </Grid>
              </Grid>
            </Grid>
          </DeputiesProvider>
        </PartiesProvider>
      </VotesResumesProvider>
    </VotesProvider>
  );
};

export default App;
