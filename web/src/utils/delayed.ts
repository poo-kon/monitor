import sleep from './sleep';

function delayed<T>(promise: Promise<T>, delayInMS = 800): Promise<T> {
  return new Promise(async (resolve, reject) => {
    const values = await Promise.all([
      promise.catch((error: any) => new Error(error)),
      sleep(delayInMS),
    ]);

    const response = values[0];

    if (response instanceof Error) reject(response);
    else resolve(response);
  });
}

export default delayed;
