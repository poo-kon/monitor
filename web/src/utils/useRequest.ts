import { useEffect, useState } from 'react';
import delayed from 'utils/delayed';

type LoadStatus = 'loading' | 'done' | 'submit' | 'error';

const useRequest = <T>(
  func: () => Promise<T>,
  onDone?: (args: T) => void,
  delay = 0
) => {
  const [status, setStatus] = useState<LoadStatus>();

  useEffect(() => {
    (async () => {
      if (status !== 'submit') return;
      setStatus('loading');

      try {
        const t = await delayed(func(), delay);
        setStatus('done');
        onDone?.(t);
      } catch (error) {
        setStatus('error');
        throw error;
      }
    })();
  }, [status, func, onDone, delay]);

  const submit = () => {
    if (status === 'loading') return;
    if (status === 'submit') return;
    setStatus('submit');
  };

  return {
    submit,
    done: status === 'done',
    loading: status === 'loading',
    failed: status === 'error',
  };
};

export default useRequest;
