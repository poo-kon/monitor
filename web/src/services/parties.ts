import axios from 'axios';
import PartyEntity from 'domain/entities/party_entity';

type PartiesResponseObject = {
  id: string;
  name: string;
  initials: string;
};

const partiesService = {
  fetchParties: async (): Promise<Array<PartyEntity>> => {
    const res = await axios.get('/parties');

    const partiesResponse: Array<PartiesResponseObject> = res.data.parties;
    return partiesResponse.map(
      (partyResponseObject) =>
        new PartyEntity(
          partyResponseObject.id,
          partyResponseObject.name,
          partyResponseObject.initials
        )
    );
  },
};

export default partiesService;
