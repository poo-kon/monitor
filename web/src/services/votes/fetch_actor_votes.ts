import axios from 'axios';
import ActorEntity from 'domain/entities/entity';
import PartyEntity from 'domain/entities/party_entity';
import PeriodEntity from 'domain/period/period_entity';
import ActorPropositionsVotingsEntity from 'domain/votes/entities/actor_proposition_votings_entity';
import VoteEntity from 'domain/votes/entities/vote_entity';

type ActorVotesRequest = {
  actor_type: string;
  actor_id: string;
  start_date: Date | null;
  end_date: Date | null;
};

type ActorVotesResponse = {
  propositions_votings: {
    proposition: {
      code: number;
      type: string;
      number: number;
      year: number;
      theme: string;
      description: string;
    };
    votings: {
      resume: string;
      objective: string;
      date: Date;
      vote: string;
    }[];
  }[];
};

const fetchActorVotes = async (
  actor: ActorEntity,
  period: PeriodEntity
): Promise<ActorPropositionsVotingsEntity> => {
  const request: ActorVotesRequest = {
    actor_type: actor instanceof PartyEntity ? 'party' : 'deputy',
    actor_id: actor.id,
    start_date: period.start,
    end_date: period.end,
  };
  const response = await axios.post('/actors-votes', request);
  const data: ActorVotesResponse = response.data;

  return {
    actor,
    propositionsVotings: data.propositions_votings.map((propositionVoting) => ({
      proposition: {
        code: propositionVoting.proposition.code,
        type: propositionVoting.proposition.type,
        number: propositionVoting.proposition.number,
        year: propositionVoting.proposition.year,
        description: propositionVoting.proposition.description,
        theme: propositionVoting.proposition.theme,
      },
      actorVotings: propositionVoting.votings.map((voting) => ({
        vote: new VoteEntity(voting.vote),
        voting: {
          resume: voting.resume,
          objective: voting.objective,
          date: voting.date,
        },
      })),
    })),
  };
};

export default fetchActorVotes;
