import axios from 'axios';
import DeputyEntity from 'domain/entities/deputy_entity';

type DeputyResponseObject = {
  id: string;
  name: string;
  party: string;
};

const deputiesService = {
  fetchDeputies: async (): Promise<Array<DeputyEntity>> => {
    const response = await axios.get('/deputies');

    const deputiesResponse: Array<DeputyResponseObject> =
      response.data.deputies;
    return deputiesResponse.map(
      (deputyResponseObject) =>
        new DeputyEntity(deputyResponseObject.id, deputyResponseObject.name)
    );
  },
};

export default deputiesService;
