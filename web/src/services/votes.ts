import axios from 'axios';
import DeputyEntity from 'domain/entities/deputy_entity';
import Entity from 'domain/entities/entity';
import PartyEntity from 'domain/entities/party_entity';
import PeriodEntity from 'domain/period/period_entity';
import VotesResumeEntity from 'domain/votes/entities/votes_resume_entity';
import fetchActorVotes from './votes/fetch_actor_votes';

type VotesResumeResponse = {
  yes: number;
  no: number;
  obstruct: number;
  null: number;
  free: number;
};

const mapVotesResumeResponseToEntity = (
  entity: Entity,
  votesResume: VotesResumeResponse
): VotesResumeEntity =>
  new VotesResumeEntity(entity, {
    yes: votesResume.yes,
    no: votesResume.no,
    obstruct: votesResume.obstruct,
    missing: votesResume.null,
    free: votesResume.free,
  });

const votesService = {
  fetchVotesResume: async (
    entities: Entity[],
    period: PeriodEntity
  ): Promise<VotesResumeEntity[]> => {
    const response = await axios.post('/votes-resume', {
      party_ids: entities
        .filter((entity) => entity instanceof PartyEntity)
        .map((party) => party.id),
      deputy_ids: entities
        .filter((entity) => entity instanceof DeputyEntity)
        .map((deputy) => deputy.id),
      start: period.start,
      end: period.end,
    });

    const deputyVotesResumes: { [key: string]: VotesResumeResponse } =
      response.data['deputy_votes_resumes'];

    const partyOrientationsResumes: { [key: string]: VotesResumeResponse } =
      response.data['party_orientations_resumes'];

    const votesResumes = entities.map((entity) =>
      mapVotesResumeResponseToEntity(
        entity,
        entity instanceof DeputyEntity
          ? deputyVotesResumes[entity.id]
          : partyOrientationsResumes[entity.id]
      )
    );

    return votesResumes;
  },
  fetchActorVotes,
};

export default votesService;
