import { Error } from '@mui/icons-material';
import { Button, Grid, Icon, Typography } from '@mui/material';
import { FC } from 'react';

type Props = {
  retry: () => void;
};
const FailedIndicatorComponent: FC<Props> = ({ retry, children }) => {
  return (
    <Grid container direction="column" alignItems="center">
      <Grid item>
        <Icon>
          <Error color="error" />
        </Icon>
      </Grid>

      <Grid item>
        <Typography variant="body1">{children}</Typography>
      </Grid>

      <Grid item>
        <Button onClick={retry} color="error" style={{ textTransform: 'none' }}>
          Tentar novamente
        </Button>
      </Grid>
    </Grid>
  );
};

export default FailedIndicatorComponent;
