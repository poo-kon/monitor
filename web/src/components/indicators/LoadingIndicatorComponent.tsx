import { CircularProgress, Grid, Typography } from '@mui/material';
import { FC } from 'react';

const LoadingIndicatorComponent: FC = ({ children }) => {
  return (
    <Grid container direction="column" alignItems="center">
      <Grid item>
        <CircularProgress />
      </Grid>

      <Grid item sx={{ marginTop: 2 }}>
        <Typography variant="body1">{children}</Typography>
      </Grid>
    </Grid>
  );
};

export default LoadingIndicatorComponent;
