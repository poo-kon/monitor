import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Entity from 'domain/entities/entity';
import EntitiesAutocompleteComponent from './EntitiesAutocompleteComponent';

describe('EntitiesAutocomplete', () => {
  it('should call submit when add button is clicked', async () => {
    const mockSubmit = jest.fn();
    const mockEntity: Entity = { name: 'mock entity', id: '1' };
    render(
      <EntitiesAutocompleteComponent
        id="test"
        label="label-test"
        submit={mockSubmit}
        loading={false}
        options={[mockEntity]}
      />
    );

    userEvent.type(screen.getByLabelText('label-test'), 'mock');
    await waitFor(() => screen.getByRole('option', { name: 'mock entity' }));
    userEvent.click(screen.getByRole('option', { name: 'mock entity' }));
    userEvent.click(screen.getByRole('button', { name: 'Adicionar' }));

    expect(mockSubmit).toBeCalledWith(mockEntity);
    expect(screen.getByLabelText('label-test')).toHaveValue('');
    expect(screen.getByRole('button', { name: 'Adicionar' })).toBeDisabled();
  });

  it('should show loadingText when loading is true', async () => {
    render(
      <EntitiesAutocompleteComponent
        id="test"
        label="label-test"
        submit={jest.fn()}
        loading={true}
        options={[]}
      />
    );

    userEvent.type(screen.getByLabelText('label-test'), 'mock');
    await waitFor(() =>
      expect(screen.getByText('Carregando opções...')).toBeVisible()
    );
  });

  it('should show error message when failed is true', async () => {
    const mockRetry = jest.fn();
    render(
      <EntitiesAutocompleteComponent
        id="test"
        label="label-test"
        submit={jest.fn}
        loading={false}
        failed={true}
        options={[]}
        retry={mockRetry}
      />
    );

    await waitFor(() =>
      expect(screen.getByText(/houve um erro no carregamento/i)).toBeVisible()
    );
    userEvent.click(screen.getByRole('button', { name: /tentar novamente/i }));
    expect(mockRetry).toBeCalled();
    expect(
      screen.queryByRole('button', { name: /adicionar/i })
    ).not.toBeInTheDocument();
    expect(screen.getByLabelText('label-test')).toBeDisabled();
  });
});
