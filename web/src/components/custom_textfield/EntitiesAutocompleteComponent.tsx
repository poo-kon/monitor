import { LoadingButton } from '@mui/lab';
import {
  Autocomplete,
  Button,
  Collapse,
  Fade,
  Grid,
  TextField,
  Typography,
} from '@mui/material';
import { Box } from '@mui/system';
import AutocompleteFilterType from 'components/custom_textfield/AutocompleteFilterType';
import Entity from 'domain/entities/entity';
import { FC, HTMLAttributes, useCallback, useState } from 'react';

type Props = {
  id: string;
  label: string;
  submit: (entity: Entity) => void;
  options?: Array<Entity>;
  loading?: boolean;
  renderListItem?: (entity: Entity) => JSX.Element;
  filterOptions?: AutocompleteFilterType;
  failed?: boolean;
  retry?: () => void;
};
const EntitiesAutocompleteComponent: FC<Props> = ({
  id,
  label,
  submit,
  options = [],
  loading = false,
  renderListItem = (entity) => (
    <Typography variant="body1">{entity.name}</Typography>
  ),
  filterOptions,
  failed = false,
  retry = () => {},
}) => {
  const [value, setValue] = useState('');
  const [selectedEntity, setSelectedEntity] = useState<Entity | null>(null);

  const onSubmit = () => {
    if (selectedEntity) {
      submit(selectedEntity);
      setSelectedEntity(null);
      setValue('');
    }
  };

  const _renderOption = useCallback(
    (props: HTMLAttributes<HTMLLIElement>, entity: Entity) => {
      return (
        <Box component="li" {...props} key={entity.id}>
          {renderListItem(entity)}
        </Box>
      );
    },
    [renderListItem]
  );

  return (
    <Grid container direction="column">
      <Grid item container>
        <Autocomplete
          fullWidth
          autoHighlight
          loading={loading}
          options={options}
          value={selectedEntity}
          filterOptions={filterOptions}
          renderOption={(props, option) => _renderOption(props, option)}
          getOptionLabel={(option) => option.name}
          onChange={(_, selectedEntity) => setSelectedEntity(selectedEntity)}
          renderInput={(params) => (
            <TextField
              {...params}
              id={id}
              label={label}
              fullWidth
              value={value}
              onChange={(event) => setValue(event.target.value)}
            />
          )}
          noOptionsText="Sem opções..."
          loadingText="Carregando opções..."
          disabled={failed}
        />
      </Grid>

      <Collapse in={!failed} unmountOnExit>
        <Grid item container sx={{ marginTop: 1 }} justifyContent="flex-end">
          <Button
            disabled={!selectedEntity}
            onClick={onSubmit}
            sx={{ paddingLeft: 0 }}
          >
            Adicionar
          </Button>
        </Grid>
      </Collapse>

      <Fade in={failed} unmountOnExit>
        <Grid item container direction="column" sx={{ marginTop: 2 }}>
          <Grid item>
            <Typography
              variant="body1"
              sx={{
                textAlign: 'end',
                color: (theme) => theme.palette.error.main,
              }}
            >
              <i>Houve um erro no carregamento das opções...</i>
            </Typography>
          </Grid>

          <Grid item alignSelf="flex-end">
            <LoadingButton
              sx={{ textTransform: 'none' }}
              loading={loading}
              onClick={retry}
              color="error"
            >
              <b>Tentar novamente</b>
            </LoadingButton>
          </Grid>
        </Grid>
      </Fade>
    </Grid>
  );
};

export default EntitiesAutocompleteComponent;
