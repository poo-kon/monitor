import { Button, Grid, TextField } from '@mui/material';
import { FC, useState } from 'react';

type Props = {
  id: string;
  label: string;
  submit: (value: string) => void;
};
const CustomTextfieldComponent: FC<Props> = ({ id, label, submit }) => {
  const [value, setValue] = useState('');

  const onSubmit = () => {
    if (value.trim()) {
      submit(value.trim());
      setValue('');
    }
  };

  return (
    <Grid container direction="column">
      <Grid item container>
        <TextField
          id={id}
          label={label}
          value={value}
          onChange={(event) => setValue(event.target.value)}
          fullWidth
        ></TextField>
      </Grid>

      <Grid item container sx={{ marginTop: 1 }} justifyContent="flex-end">
        <Button
          disabled={value.length === 0}
          onClick={onSubmit}
          sx={{ paddingLeft: 0 }}
        >
          Adicionar
        </Button>
      </Grid>
    </Grid>
  );
};

export default CustomTextfieldComponent;
