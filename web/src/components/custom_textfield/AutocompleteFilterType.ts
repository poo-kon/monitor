import { FilterOptionsState } from '@mui/core';
import Entity from 'domain/entities/entity';

type AutocompleteFilterType = (
  options: Entity[],
  state: FilterOptionsState<Entity>
) => Entity[];

export default AutocompleteFilterType;
