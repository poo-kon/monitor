import { Error, PieChart, TextFormat } from '@mui/icons-material';
import {
  Button,
  CircularProgress,
  Grid,
  Icon,
  ToggleButton,
  ToggleButtonGroup,
  Typography,
} from '@mui/material';
import Entity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import { FC, useState } from 'react';
import VotesResumesListComponent from './list/VotesResumesListComponent';
import useVotesResumesComponent from './useVotesResumesComponent';

type Props = {
  monitoredEntities: Array<Entity>;
  period: PeriodEntity;
};
const VotesResumesComponent: FC<Props> = ({ monitoredEntities, period }) => {
  const [displayType, setDisplayType] = useState('text');
  const { done, loading, votesResumes, failed, tryAgain } =
    useVotesResumesComponent(monitoredEntities, period);

  return (
    <Grid container direction="column">
      <Grid item alignSelf="flex-end" sx={{ marginBottom: 2 }}>
        <ToggleButtonGroup
          value={displayType}
          onChange={(_, value) => {
            if (value !== displayType && !!value) setDisplayType(value);
          }}
          exclusive={true}
        >
          <ToggleButton value="text">
            <TextFormat />
          </ToggleButton>
          <ToggleButton value="pieChart">
            <PieChart />
          </ToggleButton>
        </ToggleButtonGroup>
      </Grid>

      {loading && (
        <Grid item>
          <LoadingIndicator />
        </Grid>
      )}

      {failed && (
        <Grid item>
          <FailedIndicator tryAgain={tryAgain} />
        </Grid>
      )}

      {done && (
        <Grid item>
          <VotesResumesListComponent
            votesResumes={votesResumes}
            displayType={displayType}
          />
        </Grid>
      )}
    </Grid>
  );
};

const LoadingIndicator = () => {
  return (
    <Grid container direction="column" alignItems="center">
      <Grid item>
        <CircularProgress />
      </Grid>

      <Grid item sx={{ marginTop: 2 }}>
        <Typography variant="body1">
          Carregando <b>resumo</b> dos votos
        </Typography>
      </Grid>
    </Grid>
  );
};

type FailedIndicatorProps = {
  tryAgain: () => void;
};
const FailedIndicator: FC<FailedIndicatorProps> = ({ tryAgain }) => {
  return (
    <Grid container direction="column" alignItems="center">
      <Grid item>
        <Icon>
          <Error color="error" />
        </Icon>
      </Grid>

      <Grid item>
        <Typography>
          Carregamento do <b>resumo</b> dos votos <b>falhou</b>
        </Typography>
      </Grid>

      <Grid item>
        <Button
          onClick={tryAgain}
          color="error"
          style={{ textTransform: 'none' }}
        >
          Tentar novamente
        </Button>
      </Grid>
    </Grid>
  );
};

export default VotesResumesComponent;
