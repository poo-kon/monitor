import Entity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import useVotesResumes from 'providers/votes_resumes/useVotesResumes';
import { useEffect } from 'react';

const useVotesResumesComponent = (
  entities: Array<Entity>,
  period: PeriodEntity
) => {
  const { votesResumes, loadVotesResumes, retry, loading, failed, done } =
    useVotesResumes();

  useEffect(() => {
    loadVotesResumes(entities, period);
  }, [loadVotesResumes, entities, period]);

  return { votesResumes, loading, failed, done, tryAgain: retry };
};

export default useVotesResumesComponent;
