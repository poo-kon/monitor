import { FC } from 'react';
import { Cell, Legend, Pie, PieChart, ResponsiveContainer } from 'recharts';
import { VotesResumeData } from './format_votes_resume_data';

type Props = { votesResumeData: VotesResumeData[] };
const VotesResumePieChartComponent: FC<Props> = ({ votesResumeData }) => {
  return (
    <ResponsiveContainer height={240} width="100%">
      <PieChart>
        <Legend
          layout="vertical"
          align="right"
          verticalAlign="top"
          payload={votesResumeData.map((data) => ({
            id: data.label,
            value: data.label,
            color: data.color,
          }))}
        />
        <Pie
          data={votesResumeData}
          dataKey="value"
          innerRadius={60}
          outerRadius={80}
          paddingAngle={2}
          label
          animationBegin={0}
        >
          {votesResumeData.map((data) => (
            <Cell key={data.label} fill={data.color} name={data.label} />
          ))}
        </Pie>
      </PieChart>
    </ResponsiveContainer>
  );
};

export default VotesResumePieChartComponent;
