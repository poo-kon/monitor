import { Grid, Paper, Typography } from '@mui/material';
import PartyEntity from 'domain/entities/party_entity';
import VotesResumeEntity from 'domain/votes/entities/votes_resume_entity';
import { FC } from 'react';
import formatVotesResumeData from './format_votes_resume_data';
import VotesResumePieChartComponent from './VotesResumePieChartComponent';
import VotesResumeTextComponent from './VotesResumeTextComponent';

type Props = {
  votesResumes: VotesResumeEntity[];
  displayType: string;
};
const VotesResumesListComponent: FC<Props> = ({
  votesResumes,
  displayType,
}) => {
  return (
    <Grid container direction="column" spacing={5}>
      {votesResumes.map((votesResume) => {
        return (
          <Grid item key={votesResume.entity.id}>
            <VotesResumeItem
              votesResume={votesResume}
              displayType={displayType}
            />
          </Grid>
        );
      })}
    </Grid>
  );
};

type VotesResumeItemProps = {
  votesResume: VotesResumeEntity;
  displayType: string;
};
const VotesResumeItem: FC<VotesResumeItemProps> = ({
  votesResume,
  displayType,
}) => {
  const entity = votesResume.entity;
  const votesResumeData = formatVotesResumeData(votesResume);
  const noVotes = votesResumeData.length === 0;

  return (
    <Paper elevation={4} sx={{ padding: 3 }}>
      <Grid container direction="column">
        <Grid item>
          <Typography variant="h6">
            {entity instanceof PartyEntity
              ? (entity as PartyEntity).initials
              : entity.name}
          </Typography>
        </Grid>

        <Grid item sx={{ marginTop: 5 }}>
          {noVotes && (
            <Typography variant="body1" textAlign="center" color="error">
              Não há votos nesse período
            </Typography>
          )}
          {!noVotes && displayType === 'text' && (
            <VotesResumeTextComponent votesResumeData={votesResumeData} />
          )}
          {!noVotes && displayType === 'pieChart' && (
            <VotesResumePieChartComponent votesResumeData={votesResumeData} />
          )}
        </Grid>
      </Grid>
    </Paper>
  );
};

export default VotesResumesListComponent;
