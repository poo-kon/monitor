import { Grid, Typography } from '@mui/material';
import { FC } from 'react';
import { VotesResumeData } from './format_votes_resume_data';

type Props = {
  votesResumeData: VotesResumeData[];
};
const VotesResumeTextComponent: FC<Props> = ({ votesResumeData }) => {
  return (
    <Grid
      sx={{
        display: 'flex',
        flexWrap: 'wrap',
        gap: 5,
        rowGap: 2,
      }}
    >
      {votesResumeData.map((data) => {
        return (
          <VoteText
            key={data.label}
            count={data.value!}
            text={data.label}
            color={data.color}
          />
        );
      })}
    </Grid>
  );
};

type VoteTextProps = {
  text: string;
  count: number;
  color: string;
};
const VoteText: FC<VoteTextProps> = ({ text, count, color }) => {
  return (
    <Typography variant="body1" style={{ color }}>
      <b>{text}</b>
      <i style={{ marginLeft: 8 }}>{count}</i>
    </Typography>
  );
};

export default VotesResumeTextComponent;
