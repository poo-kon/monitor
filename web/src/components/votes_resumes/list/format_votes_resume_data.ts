import { colors } from '@mui/material';
import VotesResumeEntity from 'domain/votes/entities/votes_resume_entity';

export type VotesResumeData = {
  label: string;
  value?: number;
  color: string;
};

const formatVotesResumeData = (
  votesResume: VotesResumeEntity
): VotesResumeData[] =>
  [
    { label: 'Sim', color: colors.green[500], value: votesResume.yes },
    { label: 'Não', color: colors.red[500], value: votesResume.no },
    {
      label: 'Obstrução',
      color: colors.orange[500],
      value: votesResume.obstruct,
    },
    { label: 'Liberado', color: colors.blue[500], value: votesResume.free },
    { label: 'Ausente', color: colors.grey[500], value: votesResume.missing },
  ].filter((data) => !!data.value);

export default formatVotesResumeData;
