import PeriodEntity from 'domain/period/period_entity';
import validatePeriodUseCase, {
  GreaterThanEndError,
} from 'domain/period/validate_period_usecase';

const useDatePickerComponent = (period: PeriodEntity) => {
  const errors = validatePeriodUseCase(period);

  const showStartThanEndErrorMessage =
    errors?.start instanceof GreaterThanEndError;

  return {
    showStartThanEndErrorMessage,
  };
};

export default useDatePickerComponent;
