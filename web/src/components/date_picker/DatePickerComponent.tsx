import { DesktopDatePicker, LocalizationProvider } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { Fade, Stack, TextField, Typography } from '@mui/material';
import PeriodEntity from 'domain/period/period_entity';
import { FC } from 'react';
import useDatePickerComponent from './useDatePickerComponent';

type Props = {
  period: PeriodEntity;
  setPeriod: (period: PeriodEntity) => void;
};
const DatePickerComponent: FC<Props> = ({ period, setPeriod }) => {
  const { showStartThanEndErrorMessage } = useDatePickerComponent(period);

  const _onChange = (key: keyof PeriodEntity) => (date: Date | null) => {
    const isInvalidDate = !date?.getDate();
    setPeriod({ ...period, [key]: isInvalidDate ? null : date });
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Stack spacing={4}>
        <DesktopDatePicker
          label="Início"
          inputFormat="dd/MM/yyyy"
          value={period.start}
          onChange={_onChange('start')}
          renderInput={(params) => <TextField {...params} />}
        />

        <DesktopDatePicker
          label="Fim"
          inputFormat="dd/MM/yyyy"
          value={period.end}
          onChange={_onChange('end')}
          renderInput={(params) => <TextField {...params} />}
          maxDate={new Date()}
        />

        <ErrorMessage
          show={showStartThanEndErrorMessage}
          error="Data inicial não pode ser maior que a data final"
        />
      </Stack>
    </LocalizationProvider>
  );
};

type ErrorMessageProps = {
  error: string;
  show: boolean;
};
const ErrorMessage: FC<ErrorMessageProps> = ({ error, show }) => {
  return (
    <Fade in={show} unmountOnExit>
      <Typography variant="body1" color="error" textAlign="end">
        <i>{error}</i>
      </Typography>
    </Fade>
  );
};

export default DatePickerComponent;
