import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import useParties from 'providers/entities/parties/useParties';
import MonitorComponent, { stepTitles } from './MonitorComponent';

jest.mock('providers/entities/parties/useParties');

describe('MonitorComponent', () => {
  const setup = () => {
    (useParties as jest.Mock).mockReturnValue({
      parties: [{ name: 'Mock Party', id: '1', initials: 'MP' }],
    });

    render(<MonitorComponent />);

    const addEntity = async () => {
      userEvent.type(screen.getByLabelText('Partido Político'), 'p');
      userEvent.click(
        await screen.findByRole('option', { name: /mock party/i })
      );
      userEvent.click(screen.getByRole('button', { name: 'Adicionar' }));
    };

    return {
      addEntity,
    };
  };

  it('should disable next button', () => {
    setup();

    expect(
      screen.getByRole('button', { name: 'Próximo passo' })
    ).toBeDisabled();
  });

  it('should enable next button when entity is added', async () => {
    const { addEntity } = setup();
    const button = screen.getByRole('button', { name: 'Próximo passo' });

    expect(button).toBeDisabled();
    await addEntity();
    expect(button).not.toBeDisabled();
  });

  it('should move to next step when next button is clicked', async () => {
    const { addEntity } = setup();
    const button = screen.getByRole('button', { name: 'Próximo passo' });

    expect(screen.getByText(stepTitles[0])).toBeVisible();
    expect(screen.queryByText(stepTitles[1])).not.toBeInTheDocument();
    await addEntity();
    userEvent.click(button);
    expect(screen.queryByText(stepTitles[0])).not.toBeInTheDocument();
    expect(screen.getByText(stepTitles[1])).toBeVisible();
    expect(button).toBeDisabled();
  });

  it('should move to previous step when back button is clicked', async () => {
    const { addEntity } = setup();
    const nextButton = screen.getByRole('button', { name: 'Próximo passo' });

    await addEntity();
    userEvent.click(nextButton);

    const previousButton = screen.getByRole('button', { name: 'Voltar' });

    expect(screen.getByLabelText('Palavra-chave')).toBeVisible();
    userEvent.click(previousButton);
    await waitFor(() =>
      expect(
        screen.queryByRole('button', { name: 'Voltar' })
      ).not.toBeInTheDocument()
    );
  });
});
