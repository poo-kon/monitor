import Entity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import validatePeriodUseCase from 'domain/period/validate_period_usecase';
import { useMemo, useState } from 'react';

const useMonitorComponent = () => {
  const [step, setStep] = useState(0);
  const [monitoredEntities, setMonitoredEntities] = useState<Array<Entity>>([]);
  const [keywords, setKeywords] = useState<Array<string>>([]);
  const [period, setPeriod] = useState<PeriodEntity>({
    start: null,
    end: null,
  });

  const emptyMonitoredEntitiesList = monitoredEntities.length === 0;
  const emptyKeywordsList = keywords.length === 0;
  const invalidPeriod = validatePeriodUseCase(period) !== null;

  const isNextButtonDisabled = useMemo(() => {
    switch (step) {
      case 0:
        return emptyMonitoredEntitiesList;

      case 1:
        return emptyKeywordsList;

      case 2:
        return invalidPeriod;
    }
  }, [step, emptyMonitoredEntitiesList, emptyKeywordsList, invalidPeriod]);

  const goToNextStep = () => setStep(step + 1);
  const goToPreviousStep = () => setStep(step - 1);

  return {
    step,
    monitoredEntities,
    setMonitoredEntities,
    keywords,
    setKeywords,
    period,
    setPeriod,
    isNextButtonDisabled,
    goToNextStep,
    showBackButton: step !== 0,
    goToPreviousStep,
  };
};

export default useMonitorComponent;
