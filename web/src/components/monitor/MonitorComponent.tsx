import { Button, Grid, Typography } from '@mui/material';
import { Box } from '@mui/system';
import DatePickerComponent from 'components/date_picker/DatePickerComponent';
import EntityPickerComponent from 'components/entity_picker/EntityPickerComponent';
import KeywordPickerComponent from 'components/keyword_picker/KeywordPickerComponent';
import VotesComponent from 'components/votes/VotesComponent';
import VotesResumesComponent from 'components/votes_resumes/VotesResumesComponent';
import useMonitorComponent from './useMonitorComponent';

export const stepTitles = [
  'Escolha quais agentes políticos você gostaria de monitorar',
  'Liste as palavras-chave que você gostaria de monitorar',
  'Selecione um período de tempo',
  'Resumo dos votos dos agentes políticos selecionados',
  'Votos dos agentes políticos selecionados',
];

const MonitorComponent = () => {
  const {
    step,
    monitoredEntities,
    setMonitoredEntities,
    keywords,
    setKeywords,
    period,
    setPeriod,
    isNextButtonDisabled,
    goToNextStep,
    showBackButton,
    goToPreviousStep,
  } = useMonitorComponent();
  const title = stepTitles[step];

  return (
    <Grid container direction="column">
      <Grid item container justifyContent="center">
        <Grid item xs={12} sm={10} md={8} lg={7}>
          <Typography variant="body1" textAlign="center">
            {title}
          </Typography>
        </Grid>
      </Grid>

      <Grid item container sx={{ marginTop: 5 }} justifyContent="center">
        <Grid item xs={12} sm={10} md={8} lg={7}>
          {step === 0 && (
            <EntityPickerComponent
              entities={monitoredEntities}
              setEntities={setMonitoredEntities}
            />
          )}

          {step === 1 && (
            <Box sx={{ marginTop: 8 }}>
              <KeywordPickerComponent
                keywords={keywords}
                setKeywords={setKeywords}
              />
            </Box>
          )}

          {step === 2 && (
            <Box sx={{ marginTop: 8 }}>
              <DatePickerComponent period={period} setPeriod={setPeriod} />
            </Box>
          )}

          {step === 3 && (
            <Box sx={{ marginTop: 8 }}>
              <VotesResumesComponent
                monitoredEntities={monitoredEntities}
                period={period}
              />
            </Box>
          )}

          {step === 4 && (
            <Box sx={{ marginTop: 8 }}>
              <VotesComponent
                actors={monitoredEntities}
                keywords={keywords}
                period={period}
              />
            </Box>
          )}
        </Grid>
      </Grid>

      <Grid item container sx={{ marginTop: 8 }} justifyContent="center">
        <Grid
          container
          item
          xs={12}
          sm={10}
          md={8}
          lg={7}
          justifyContent="flex-end"
        >
          {showBackButton && (
            <Grid item style={{ flexGrow: 1 }}>
              <Button onClick={goToPreviousStep}>Voltar</Button>
            </Grid>
          )}
          <Grid item>
            <Button disabled={isNextButtonDisabled} onClick={goToNextStep}>
              Próximo passo
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default MonitorComponent;
