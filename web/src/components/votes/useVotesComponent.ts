import ActorEntity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import ActorPropositionsVotingsEntity from 'domain/votes/entities/actor_proposition_votings_entity';
import useVotes from 'providers/votes/useVotes';
import { useEffect, useState } from 'react';

type Filter = {
  actorID: string;
  keyword: string;
};
const useVotesComponent = (actors: ActorEntity[], period: PeriodEntity) => {
  const [selectedKeyword, setSelectedKeyword] = useState<string>('');
  const [selectedActorID, setSelectedActorID] = useState<string>('');
  const [lastFilterComputed, setLastFilterComputed] = useState<Filter>({
    actorID: '',
    keyword: '',
  });
  const [selectedActorPropositionsVotings, setActorPropositionsVotings] =
    useState<ActorPropositionsVotingsEntity>();
  const { loadVotes, retry, actorsPropositionsVotings, done, loading, failed } =
    useVotes();

  useEffect(() => {
    loadVotes(actors, period);
  }, [loadVotes, actors, period]);

  useEffect(() => {
    const allItems = actorsPropositionsVotings[selectedActorID];

    if (!allItems) return;
    if (
      lastFilterComputed.actorID === selectedActorID &&
      lastFilterComputed.keyword === selectedKeyword
    )
      return;

    const pattern = selectedKeyword;
    const _matches = (text: string, pattern: string) =>
      text.search(pattern) !== -1;

    setLastFilterComputed({
      actorID: selectedActorID,
      keyword: selectedKeyword,
    });
    setActorPropositionsVotings({
      actor: allItems.actor,
      propositionsVotings: allItems.propositionsVotings.filter(
        (voting) =>
          _matches(voting.proposition.description, pattern) ||
          _matches(voting.proposition.theme, pattern)
      ),
    });
  }, [
    actorsPropositionsVotings,
    lastFilterComputed,
    selectedActorID,
    selectedKeyword,
  ]);

  return {
    selectedKeyword,
    setSelectedKeyword,
    selectedActorID,
    setSelectedActorID,
    selectedActorPropositionsVotings,
    retry,
    done,
    loading,
    failed,
  };
};

export default useVotesComponent;
