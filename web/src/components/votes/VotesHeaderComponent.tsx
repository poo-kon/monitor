import { Clear } from '@mui/icons-material';
import {
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
} from '@mui/material';
import ActorEntity from 'domain/entities/entity';
import { FC } from 'react';

type Props = {
  actorID: string;
  setActorID: (actorID: string) => void;
  monitoredActors: ActorEntity[];
  keyword: string;
  setKeyword: (keyword: string) => void;
  keywords: string[];
};
const VotesHeaderComponent: FC<Props> = ({
  actorID,
  setActorID,
  monitoredActors,
  keyword,
  setKeyword,
  keywords,
}) => {
  const resetKeyword = () => setKeyword('');

  return (
    <Grid container spacing={4} justifyContent="flex-end">
      <Grid item xs={12} sm={6}>
        <FormControl fullWidth>
          <InputLabel id="actors-select-label">Agente Político</InputLabel>
          <Select
            labelId="actors-select-label"
            id="actors-select"
            value={actorID}
            onChange={(event) => setActorID(event.target.value)}
            label="Agente Político"
          >
            {monitoredActors.map((monitoredActor) => (
              <MenuItem key={monitoredActor.id} value={monitoredActor.id}>
                {monitoredActor.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>

      <Grid item xs={12} sm={6}>
        <FormControl fullWidth>
          <InputLabel id="keyword-label">Palavra-Chave</InputLabel>
          <IconButton
            onClick={resetKeyword}
            size="small"
            sx={{
              margin: 1,
              position: 'absolute',
              right: 0,
              zIndex: 999,
              marginTop: 1.5,
            }}
          >
            <Clear fontSize="small" />
          </IconButton>
          <Select
            labelId="actors-select-label"
            id="actors-select"
            value={keyword}
            onChange={(event) => setKeyword(event.target.value)}
            label="Agente Político"
            IconComponent={() => <IconButton sx={{ display: 'none' }} />}
          >
            {keywords.map((keyword) => (
              <MenuItem key={keyword} value={keyword}>
                {keyword}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  );
};

export default VotesHeaderComponent;
