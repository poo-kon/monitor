import { Grid, Typography } from '@mui/material';
import ActorPropositionsVotingsEntity from 'domain/votes/entities/actor_proposition_votings_entity';
import { FC } from 'react';
import PropositionCardComponent from './PropositionCardComponent';

type Props = {
  actorPropositionsVotings?: ActorPropositionsVotingsEntity;
};
const VotesListComponent: FC<Props> = ({ actorPropositionsVotings }) => {
  const noActorSelected = !actorPropositionsVotings;

  if (noActorSelected) return <NoActorSelected />;

  return (
    <Grid container direction="column" spacing={5}>
      {actorPropositionsVotings.propositionsVotings.map(
        (propositionVotings) => {
          return (
            <Grid item key={propositionVotings.proposition.code}>
              <PropositionCardComponent
                proposition={propositionVotings.proposition}
                votingsVote={propositionVotings.actorVotings}
              />
            </Grid>
          );
        }
      )}
    </Grid>
  );
};

const NoActorSelected = () => {
  return (
    <Grid container direction="column" alignItems="center">
      <Grid item>
        <Typography variant="body1">
          <b>Selecione</b> um agente
        </Typography>
      </Grid>

      <Grid item sx={{ marginTop: 0.25 }}>
        <Typography variant="body1">
          para ver os seus <b>votos</b>
        </Typography>
      </Grid>
    </Grid>
  );
};

export default VotesListComponent;
