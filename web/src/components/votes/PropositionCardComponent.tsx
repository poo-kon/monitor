import { Grid, Paper, Typography } from '@mui/material';
import PropositionEntity from 'domain/votes/entities/proposition_entity';
import VoteEntity from 'domain/votes/entities/vote_entity';
import VotingEntity from 'domain/votes/entities/voting_entity';
import { FC } from 'react';
import VotingItemComponent from './VotingItemComponent';

type Props = {
  proposition: PropositionEntity;
  votingsVote: {
    voting: VotingEntity;
    vote: VoteEntity;
  }[];
};
const PropositionCardComponent: FC<Props> = ({ proposition, votingsVote }) => {
  const propostionID = `${proposition.type} ${proposition.number}/${proposition.year}`;

  return (
    <Paper elevation={4} sx={{ padding: 4 }}>
      <Grid container direction="column">
        <Grid item sx={{ marginBottom: 2 }}>
          <Typography variant="body1">
            <b>{propostionID}</b>
          </Typography>
        </Grid>

        <Grid item sx={{ marginBottom: 2 }}>
          <Typography variant="body1">{proposition.theme}</Typography>
        </Grid>

        <Grid item container direction="column" spacing={1}>
          {votingsVote.map((votingVote, index) => (
            <Grid item key={index}>
              <VotingItemComponent
                vote={votingVote.vote}
                voting={votingVote.voting}
              />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Paper>
  );
};

export default PropositionCardComponent;
