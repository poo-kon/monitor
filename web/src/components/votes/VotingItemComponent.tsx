import { Grid } from '@mui/material';
import VoteEntity from 'domain/votes/entities/vote_entity';
import VotingEntity from 'domain/votes/entities/voting_entity';
import { FC } from 'react';

type Props = {
  vote: VoteEntity;
  voting: VotingEntity;
};
const VotingItemComponent: FC<Props> = ({ vote, voting }) => {
  return <Grid container>{vote.value}</Grid>;
};

export default VotingItemComponent;
