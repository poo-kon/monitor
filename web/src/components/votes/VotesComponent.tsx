import { Grid } from '@mui/material';
import FailedIndicatorComponent from 'components/indicators/FailedIndicatorComponent';
import LoadingIndicatorComponent from 'components/indicators/LoadingIndicatorComponent';
import ActorEntity from 'domain/entities/entity';
import PeriodEntity from 'domain/period/period_entity';
import { FC } from 'react';
import useVotesComponent from './useVotesComponent';
import VotesHeaderComponent from './VotesHeaderComponent';
import VotesListComponent from './VotesListComponent';

type Props = {
  actors: ActorEntity[];
  period: PeriodEntity;
  keywords: string[];
};
const VotesComponent: FC<Props> = ({ actors, period, keywords }) => {
  const {
    selectedKeyword,
    setSelectedKeyword,
    selectedActorID,
    setSelectedActorID,
    selectedActorPropositionsVotings,
    retry,
    done,
    loading,
    failed,
  } = useVotesComponent(actors, period);

  return (
    <Grid container direction="column">
      <Grid item sx={{ marginBottom: 10 }}>
        <VotesHeaderComponent
          actorID={selectedActorID}
          setActorID={setSelectedActorID}
          monitoredActors={actors}
          keyword={selectedKeyword}
          setKeyword={setSelectedKeyword}
          keywords={keywords}
        />
      </Grid>

      {loading && (
        <Grid item>
          <LoadingIndicatorComponent>
            Carregando <b>votos</b>
          </LoadingIndicatorComponent>
        </Grid>
      )}

      {failed && (
        <Grid item>
          <FailedIndicatorComponent retry={retry}>
            Carregamento dos votos <b>falhou</b>
          </FailedIndicatorComponent>
        </Grid>
      )}

      {done && (
        <Grid item>
          <VotesListComponent
            actorPropositionsVotings={selectedActorPropositionsVotings}
          />
        </Grid>
      )}
    </Grid>
  );
};

export default VotesComponent;
