import { createFilterOptions } from '@mui/core';
import AutocompleteFilterType from 'components/custom_textfield/AutocompleteFilterType';
import PartyEntity from 'domain/entities/party_entity';

const partyFilterOptions = createFilterOptions<PartyEntity>({
  stringify: (option) => `${option.initials} ${option.name}`,
});

export default partyFilterOptions as unknown as AutocompleteFilterType;
