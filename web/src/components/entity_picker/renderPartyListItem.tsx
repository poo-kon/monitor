import { Typography } from '@mui/material';
import Entity from 'domain/entities/entity';
import PartyEntity from 'domain/entities/party_entity';

const renderPartyListItem = (entity: Entity) => {
  const party = entity as PartyEntity;

  return (
    <Typography variant="body1">
      <b>{party.initials}</b> &middot; <i>{party.name}</i>
    </Typography>
  );
};

export default renderPartyListItem;
