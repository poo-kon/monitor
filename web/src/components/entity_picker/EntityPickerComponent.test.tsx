import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Entity from 'domain/entities/entity';
import useDeputies from 'providers/entities/deputies/useDeputies';
import useParties from 'providers/entities/parties/useParties';
import { useState } from 'react';
import EntityPickerComponent from './EntityPickerComponent';

jest.mock('providers/entities/parties/useParties');
jest.mock('providers/entities/deputies/useDeputies');

type Tab = 'deputies' | 'parties';

const WrapperComponent = () => {
  const [entities, setEntities] = useState<Array<Entity>>([]);

  return (
    <EntityPickerComponent entities={entities} setEntities={setEntities} />
  );
};

describe('EntityPickerComponent', () => {
  const tabs = {
    deputies: 'Deputados',
    parties: 'Partidos',
  };

  const labels = {
    deputies: 'Deputado Federal',
    parties: 'Partido Político',
  };

  const setup = () => {
    (useDeputies as jest.Mock).mockReturnValue({
      deputies: [{ name: 'Mock Deputy', id: '1' }],
    });

    (useParties as jest.Mock).mockReturnValue({
      parties: [{ name: 'Mock Party', id: '2' }],
    });

    render(<WrapperComponent />);

    const deputiesTab = screen.getByText(tabs.deputies);
    const partiesTab = screen.getByText(tabs.parties);

    return {
      deputiesTab,
      partiesTab,
    };
  };

  const addEntity = async (
    tab: Tab,
    option: string | RegExp,
    inputText: string
  ) => {
    userEvent.click(screen.getByText(tabs[tab]));
    userEvent.type(screen.getByLabelText(labels[tab]), inputText);
    userEvent.click(await screen.findByRole('option', { name: option }));
    userEvent.click(screen.getByRole('button', { name: 'Adicionar' }));
  };

  it('should render tabs', () => {
    setup();
  });

  it('should change tabs', () => {
    const { deputiesTab, partiesTab } = setup();

    userEvent.click(partiesTab);
    screen.getByLabelText(labels.parties);

    userEvent.click(deputiesTab);
    screen.getByLabelText(labels.deputies);
  });

  it('should reset input when tabs are changed', async () => {
    const { deputiesTab, partiesTab } = setup();

    userEvent.click(partiesTab);
    userEvent.type(screen.getByLabelText(labels.parties), 'abcdef');

    userEvent.click(deputiesTab);
    await waitFor(() =>
      expect(screen.getByLabelText(labels.deputies)).toHaveValue('')
    );

    userEvent.type(screen.getByLabelText(labels.deputies), '123456');

    userEvent.click(partiesTab);
    await waitFor(() =>
      expect(screen.getByLabelText(labels.parties)).toHaveValue('')
    );
  });

  it('should add entity to list when submit button is clicked', async () => {
    setup();

    await addEntity('deputies', 'Mock Deputy', 'dep');
    await waitFor(() => expect(screen.getByText('Mock Deputy')).toBeVisible());
  });

  it('should show delete menu when delete button is clicked', async () => {
    setup();

    await addEntity('parties', /mock party/i, 'rty');
    await waitFor(() => expect(screen.getByLabelText('delete')).toBeVisible());

    userEvent.click(screen.getByLabelText('delete'));

    await waitFor(() =>
      expect(screen.getByLabelText('delete-menu-item')).toBeVisible()
    );
  });

  it('should remove item when delete menu is clicked', async () => {
    const PARTY_NAME = 'Mock Party';
    setup();

    await addEntity('parties', /mock party/i, 'par');
    await waitFor(() => expect(screen.getByText(PARTY_NAME)).toBeVisible());

    await waitFor(() => expect(screen.getByLabelText('delete')).toBeVisible());
    userEvent.click(screen.getByLabelText('delete'));

    await waitFor(() =>
      expect(screen.getByLabelText('delete-menu-item')).toBeVisible()
    );
    userEvent.click(screen.getByLabelText('delete-menu-item'));

    await waitFor(() =>
      expect(screen.queryByText(PARTY_NAME)).not.toBeInTheDocument()
    );
  });
});
