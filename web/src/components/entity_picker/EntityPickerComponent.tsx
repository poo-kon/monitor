import {
  Grid,
  ToggleButton,
  ToggleButtonGroup,
  Typography,
} from '@mui/material';
import CustomListComponent from 'components/custom_list/CustomListComponent';
import EntitiesAutocompleteComponent from 'components/custom_textfield/EntitiesAutocompleteComponent';
import Entity from 'domain/entities/entity';
import useDeputies from 'providers/entities/deputies/useDeputies';
import useParties from 'providers/entities/parties/useParties';
import { FC, useState } from 'react';
import partyFilterOptions from './partyFilterOptions';
import renderPartyListItem from './renderPartyListItem';

const placeholders = ['Partido Político', 'Deputado Federal'];

type Props = {
  entities: Array<Entity>;
  setEntities: (entities: Array<Entity>) => void;
};
const EntityPickerComponent: FC<Props> = ({ entities, setEntities }) => {
  const [tab, setTab] = useState(0);
  const deputiesHook = useDeputies();
  const partiesHook = useParties();

  const addEntity = (entity: Entity) => setEntities(entities.concat(entity));

  const deleteEntity = (deletedEntity: Entity) =>
    setEntities(entities.filter((entity) => entity !== deletedEntity));

  return (
    <Grid container direction="column">
      <Grid item container justifyContent="flex-end">
        <ToggleButtonGroup
          value={tab}
          onChange={(_, newValue) => {
            if (newValue === null) return;
            if (newValue === tab) return;
            setTab(newValue);
          }}
          exclusive={true}
        >
          <ToggleButton value={0}>Partidos</ToggleButton>
          <ToggleButton value={1}>Deputados</ToggleButton>
        </ToggleButtonGroup>
      </Grid>

      <Grid container sx={{ marginTop: 2 }}>
        <EntitiesAutocompleteComponent
          id="entity-textfield"
          key={placeholders[tab]}
          label={placeholders[tab]}
          submit={addEntity}
          loading={!tab ? partiesHook.loading : deputiesHook.loading}
          options={!tab ? partiesHook.parties : deputiesHook.deputies}
          renderListItem={!tab ? renderPartyListItem : undefined}
          filterOptions={!tab ? partyFilterOptions : undefined}
          failed={!tab ? partiesHook.failed : deputiesHook.failed}
          retry={!tab ? partiesHook.loadParties : deputiesHook.loadDeputies}
        />
      </Grid>

      <Grid container sx={{ marginTop: 3 }}>
        <CustomListComponent
          items={entities}
          deleteItem={deleteEntity}
          itemKey={(item) => item.id}
          renderItem={(item) => (
            <Typography variant="body1">{item.name}</Typography>
          )}
        />
      </Grid>
    </Grid>
  );
};

export default EntityPickerComponent;
