import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { useState } from 'react';
import KeywordPickerComponent from './KeywordPickerComponent';

const WrapperComponent = () => {
  const [keywords, setKeywords] = useState<Array<string>>([]);

  return (
    <KeywordPickerComponent keywords={keywords} setKeywords={setKeywords} />
  );
};

describe('KeywordPickerComponent', () => {
  const setup = () => {
    render(<WrapperComponent />);
  };

  it('should add keyword to list', () => {
    setup();
    const input = screen.getByLabelText('Palavra-chave');
    const button = screen.getByRole('button', { name: 'Adicionar' });
    const KEYWORD = 'ABCDEF';

    userEvent.type(input, KEYWORD);
    userEvent.click(button);

    expect(screen.getByText(KEYWORD)).toBeVisible();
  });
});
