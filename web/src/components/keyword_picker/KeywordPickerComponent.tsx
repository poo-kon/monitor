import { Grid } from '@mui/material';
import CustomListComponent from 'components/custom_list/CustomListComponent';
import CustomTextfieldComponent from 'components/custom_textfield/CustomTextfieldComponent';
import { FC } from 'react';

type Props = {
  keywords: Array<string>;
  setKeywords: (keywords: Array<string>) => void;
};
const KeywordPickerComponent: FC<Props> = ({ keywords, setKeywords }) => {
  const addKeyword = (keyword: string) =>
    setKeywords([keyword].concat(keywords));

  const deleteKeyword = (deletedKeyword: string) =>
    setKeywords(keywords.filter((keyword) => keyword !== deletedKeyword));

  return (
    <Grid container direction="column">
      <Grid item container>
        <CustomTextfieldComponent
          id="keyword-textfield"
          label="Palavra-chave"
          submit={addKeyword}
        />
      </Grid>

      <Grid item container sx={{ marginTop: 3 }}>
        <CustomListComponent
          items={keywords}
          itemKey={(item) => item}
          deleteItem={deleteKeyword}
          renderItem={(item) => <div>{item}</div>}
        />
      </Grid>
    </Grid>
  );
};

export default KeywordPickerComponent;
