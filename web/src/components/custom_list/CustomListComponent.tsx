import { HighlightOffRounded } from '@mui/icons-material';
import { Grid, IconButton, Paper, Theme } from '@mui/material';
import DeleteMenuComponent from 'components/delete_menu/DeleteMenuComponent';
import { Key } from 'react';

type Props<T> = {
  items: Array<T>;
  itemKey: (item: T) => Key;
  deleteItem: (item: T) => void;
  renderItem: (item: T) => JSX.Element;
};
const CustomListComponent = <T,>({
  items,
  itemKey,
  deleteItem,
  renderItem,
}: Props<T>) => {
  return (
    <Grid container direction="column" spacing={2}>
      {items.map((item) => (
        <Grid key={itemKey(item)} item>
          <Paper
            elevation={4}
            sx={{
              padding: 2,
            }}
          >
            <Grid container alignItems="center">
              <Grid item sx={{ flexGrow: 1 }}>
                {renderItem(item)}
              </Grid>

              <Grid item>
                <DeleteMenuComponent
                  renderButton={(onClick) => {
                    return (
                      <IconButton
                        aria-label="delete"
                        onClick={onClick}
                        sx={{
                          color: (theme: Theme) => theme.palette.error.main,
                        }}
                      >
                        <HighlightOffRounded></HighlightOffRounded>
                      </IconButton>
                    );
                  }}
                  onDeleteMenuClick={() => deleteItem(item)}
                />
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      ))}
    </Grid>
  );
};

export default CustomListComponent;
