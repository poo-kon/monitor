import { Menu, MenuItem, Typography } from '@mui/material';
import { FC, Fragment, MouseEvent, useState } from 'react';

type Props = {
  renderButton: (
    openMenu: (event: MouseEvent<HTMLElement>) => void
  ) => JSX.Element;
  onDeleteMenuClick: () => void;
};
const DeleteMenuComponent: FC<Props> = ({
  renderButton,
  onDeleteMenuClick,
}) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = !!anchorEl;

  const openMenu = (event: MouseEvent<HTMLElement>) =>
    setAnchorEl(event.currentTarget);

  const closeMenu = () => setAnchorEl(null);

  return (
    <Fragment>
      {renderButton(openMenu)}

      <Menu
        open={open}
        onClose={closeMenu}
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'center', horizontal: 'right' }}
        transformOrigin={{ vertical: 'center', horizontal: 'right' }}
      >
        <MenuItem aria-label="delete-menu-item" onClick={onDeleteMenuClick}>
          <Typography variant="body1">
            Clique novamente para <b>remover</b> esse item
          </Typography>
        </MenuItem>
      </Menu>
    </Fragment>
  );
};

export default DeleteMenuComponent;
