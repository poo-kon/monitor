# **Local Installation**

In order to run the web client locally, you need to install the following dependencies:
- [NodeJS](https://nodejs.org/en/): programming language
- [NPM](https://www.npmjs.com/): package manager

After installing the dependencies above, run ``npm install`` to download the remaining dependencies.

## **Run locally**

Run ``npm run start`` to start the web client in development mode.\
The site will be served on [http://localhost:3000](http://localhost:3000).

The page will reload if you make edits.\
You will also see any lint errors in the console.

## **Linting**

Run ``npm run lint`` to check if files are formatted correctly and if there are syntax errors.\
Run ``npm run lint-fix`` to fix lint errors.

## **Tests**

Test files in this project have the following naming patttern: **[file_to_test].test.[ts|tsx]**.\
These files are placed in the same folder of the file that are being tested.

To run the tests, run ``npm run test``.