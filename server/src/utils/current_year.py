from datetime import datetime


def current_year() -> int:
    return datetime.now().year
