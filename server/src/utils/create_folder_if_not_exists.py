from pathlib import Path


def create_folder_if_not_exists(path: str = ""):
    Path(path).mkdir(parents=True, exist_ok=True)
