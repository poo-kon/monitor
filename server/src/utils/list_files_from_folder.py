from os import listdir
from os.path import isfile, join
from typing import List


def list_files_from_folder(folder_path: str) -> List[str]:
    def __file_path(file_name: str) -> str:
        return join(folder_path, file_name)

    return [
        __file_path(file)
        for file in listdir(folder_path)
        if isfile(
            __file_path(file),
        )
    ]
