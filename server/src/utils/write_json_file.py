import json


def write_json_file(path: str, data_json: dict):
    file = open(path, "w")
    json.dump(
        data_json,
        file,
        sort_keys=True,
        indent=2,
        ensure_ascii=False,
    )
    file.close()
