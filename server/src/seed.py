import logging
import os

from domain.usecases import SeedDeputiesUseCase, SeedPartiesUseCase, SeedPropositionsUseCase, SeedVotingUseCase
from services import (
    ServiceDeputyRepository,
    ServicePartyRepository,
    ServicePropositionRepository,
    ServiceVotingRepository,
)

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))


def seed_parties():
    SeedPartiesUseCase(ServicePartyRepository()).run()


def seed_deputies():
    SeedDeputiesUseCase(ServiceDeputyRepository()).run()


def seed_propositions():
    SeedPropositionsUseCase(ServicePropositionRepository()).run()


def seed_votings():
    SeedVotingUseCase(
        voting_repository=ServiceVotingRepository(),
        proposition_repository=ServicePropositionRepository(),
    ).run()


if __name__ == "__main__":
    seed_parties()
    seed_deputies()
    seed_propositions()
    seed_votings()
