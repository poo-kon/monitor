import pytest
from domain.entities import VotesResumeEntity


@pytest.mark.parametrize(
    "vote_resume, yes, no, free, obstruct, null",
    [
        (VotesResumeEntity(yes=5, free=4), 5, 0, 4, 0, 0),
        (VotesResumeEntity(null=5, yes=2, free=4), 2, 0, 4, 0, 5),
    ],
)
def test_votes_resume_entity(
    vote_resume: VotesResumeEntity,
    yes: int,
    no: int,
    free: int,
    obstruct: int,
    null: int,
):
    assert vote_resume.yes == yes
    assert vote_resume.no == no
    assert vote_resume.free == free
    assert vote_resume.obstruct == obstruct
    assert vote_resume.null == null
