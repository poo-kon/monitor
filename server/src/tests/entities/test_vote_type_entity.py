from domain.entities import VoteTypeEntity


def test_vote_type_entity_types_method():
    assert VoteTypeEntity.types() == ["PL", "PEC"]
