from datetime import datetime
from unittest.mock import Mock

import pytest
from domain.entities import PeriodEntity, VotingEntity


@pytest.mark.parametrize(
    "period, expected_in_period",
    [
        (PeriodEntity(), True),
        (
            PeriodEntity(
                start_date=datetime(2011, 1, 12),
                end_date=datetime(2012, 1, 2),
            ),
            False,
        ),
        (
            PeriodEntity(
                start_date=datetime(2008, 1, 12),
                end_date=datetime(2009, 1, 2),
            ),
            False,
        ),
        (
            PeriodEntity(
                start_date=datetime(2010, 11, 2),
                end_date=datetime(2010, 12, 6),
            ),
            True,
        ),
        (
            PeriodEntity(
                start_date=datetime(2010, 12, 5),
                end_date=datetime(2010, 12, 5),
            ),
            True,
        ),
    ],
)
def test_voting_entity_in_period(period: PeriodEntity, expected_in_period: bool):
    voting = VotingEntity(
        resume=Mock(),
        time=Mock(),
        objective=Mock(),
        proposition=Mock(),
        date=datetime(2010, 12, 5),
    )

    assert voting.in_period(period) == expected_in_period
