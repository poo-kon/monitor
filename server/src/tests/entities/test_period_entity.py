from datetime import datetime

import pytest
from domain.entities import PeriodEntity


def test_period_entity_validate():
    start_date = datetime(2000, 12, 1)
    end_date = datetime(1999, 12, 1)

    with pytest.raises(ValueError) as error:
        PeriodEntity(start_date, end_date)

    assert str(error.value) == f"start_date({start_date}) > end_date({end_date})"


def test_period_entity():
    start_date = datetime(2000, 12, 12)
    end_date = datetime(2001, 12, 12)
    period = PeriodEntity(start_date, end_date)

    assert period.start_date == start_date
    assert period.end_date == end_date
