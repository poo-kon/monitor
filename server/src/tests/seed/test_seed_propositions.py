from unittest.mock import Mock

import dados_abertos
import pytest
from utils.read_json_file import read_json_file

from seed import seed_propositions


def mock_fetch_propositions_side_effect(year: int):
    mock_response = Mock()

    mock_response.text = {
        2015: """
            <proposicoes>
                <proposicao>
                    <codProposicao>14493</codProposicao>
                    <nomeProposicao>PEC 171/1993</nomeProposicao>
                    <dataVotacao>01/07/2015</dataVotacao>
                </proposicao>
                <proposicao>
                    <codProposicao>14493</codProposicao>
                    <nomeProposicao>PEC 171/1993</nomeProposicao>
                    <dataVotacao>01/07/2015</dataVotacao>
                </proposicao>
                <proposicao>
                    <codProposicao>14496</codProposicao>
                    <nomeProposicao>PEC 172/1994</nomeProposicao>
                    <dataVotacao>01/08/2015</dataVotacao>
                </proposicao>
            </proposicoes>
        """,
        2019: """
            <proposicoes>
                <proposicao>
                    <codProposicao>16526</codProposicao>
                    <nomeProposicao>PL 1292/1995</nomeProposicao>
                    <dataVotacao>17/06/2019</dataVotacao>
                </proposicao>
            </proposicoes>
        """,
    }.get(year, "<proposicoes></proposicoes>")

    return mock_response


@pytest.fixture(autouse=True)
def mock_fetch_propositions(monkeypatch):
    mock_fetch_propositions = Mock()

    monkeypatch.setattr(dados_abertos, "fetch_propositions", mock_fetch_propositions)

    mock_fetch_propositions.side_effect = mock_fetch_propositions_side_effect

    return mock_fetch_propositions


def mock_fetch_proposition_details_side_effect(type: str, number: int, year: int):
    mock_response = Mock()

    mock_response.status_code = 200
    mock_response.text = {
        (
            "PEC",
            171,
            1993,
        ): """
            <proposicao tipo="PEC " numero="171" ano="1993">
                <tipoProposicao>PEC</tipoProposicao>
                <tema>Política, Partidos e Eleições</tema>
                <Ementa>Altera Lei</Ementa>
                <Autor>Senado Federal - A</Autor>
                <partidoAutor>PSD </partidoAutor>
            </proposicao>
        """,
        (
            "PEC",
            172,
            1994,
        ): """
            <proposicao tipo="PEC " numero="172" ano="1994">
                <tipoProposicao>PEC</tipoProposicao>
                <tema>Meio-Ambiente</tema>
                <Ementa>Modifica Lei</Ementa>
                <Autor>Senado Federal - B</Autor>
                <partidoAutor>PT </partidoAutor>
            </proposicao>
        """,
        (
            "PL",
            1292,
            1995,
        ): """
            <proposicao tipo="PL " numero="1202" ano="1995">
                <tipoProposicao>Projeto de Lei</tipoProposicao>
                <tema>Gastos públicos</tema>
                <Ementa>Muda Lei</Ementa>
                <Autor>Senado Federal - C</Autor>
                <partidoAutor>PSOL </partidoAutor>
            </proposicao>
        """,
    }.get((type, number, year), "")

    return mock_response


@pytest.fixture(autouse=True)
def mock_fetch_proposition_details(monkeypatch):
    mock_fetch_proposition_details = Mock()

    monkeypatch.setattr(dados_abertos, "fetch_proposition_details", mock_fetch_proposition_details)

    mock_fetch_proposition_details.side_effect = mock_fetch_proposition_details_side_effect

    return mock_fetch_proposition_details


def test_seed_propositions(mock_fetch_propositions: Mock, test_memory_folder_path: str):
    seed_propositions()

    mock_fetch_propositions.assert_called()

    propositions_file_path = f"{test_memory_folder_path}/propositions.json"
    propositions = read_json_file(propositions_file_path)

    assert propositions == {
        "14493": {
            "code": 14493,
            "number": 171,
            "type": "PEC",
            "vote_date": "01/07/2015",
            "year": 1993,
        },
        "14496": {
            "code": 14496,
            "number": 172,
            "type": "PEC",
            "vote_date": "01/08/2015",
            "year": 1994,
        },
        "16526": {
            "code": 16526,
            "number": 1292,
            "type": "PL",
            "vote_date": "17/06/2019",
            "year": 1995,
        },
    }


@pytest.mark.parametrize(
    "proposition_code, expected_propostion_details_json",
    [
        (
            "14493",
            {
                "author": "Senado Federal - A",
                "description": "Altera Lei",
                "party_author": "PSD",
                "proposition_code": 14493,
                "theme": "Política, Partidos e Eleições",
            },
        ),
        (
            "14496",
            {
                "author": "Senado Federal - B",
                "description": "Modifica Lei",
                "party_author": "PT",
                "proposition_code": 14496,
                "theme": "Meio-Ambiente",
            },
        ),
        (
            "16526",
            {
                "author": "Senado Federal - C",
                "description": "Muda Lei",
                "party_author": "PSOL",
                "proposition_code": 16526,
                "theme": "Gastos públicos",
            },
        ),
    ],
)
def test_proposition_details(
    proposition_code: str,
    test_memory_folder_path: str,
    expected_propostion_details_json: dict,
):
    seed_propositions()

    proposition_details_file_path = f"{test_memory_folder_path}/propositions_details/{proposition_code}.json"
    proposition_details = read_json_file(proposition_details_file_path)

    assert proposition_details == expected_propostion_details_json
