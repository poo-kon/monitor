from unittest.mock import Mock

import dados_abertos
import pytest
from settings import Settings
from utils import read_json_file

from seed import seed_parties


@pytest.fixture(autouse=True)
def mock_fetch_parties(monkeypatch):
    mock_fetch_parties = Mock()
    mock_response = Mock()

    monkeypatch.setattr(dados_abertos, "fetch_parties", mock_fetch_parties)

    mock_fetch_parties.return_value = mock_response
    mock_response.json.return_value = {
        "dados": [
            {
                "id": 36898,
                "sigla": "AVANTE",
                "nome": "Avante",
                "uri": "https://dadosabertos.camara.leg.br/api/v2/partidos/36898",
            },
            {
                "id": 37905,
                "sigla": "CIDADANIA",
                "nome": "Cidadania",
                "uri": "https://dadosabertos.camara.leg.br/api/v2/partidos/37905",
            },
            {
                "id": 37902,
                "sigla": "DC",
                "nome": "Democracia Cristã",
                "uri": "https://dadosabertos.camara.leg.br/api/v2/partidos/37902",
            },
        ]
    }

    return mock_fetch_parties


def test_parties_seed(mock_fetch_parties: Mock):
    seed_parties()

    mock_fetch_parties.assert_called()

    memory_folder_path = Settings().memory_folder_path
    parties_file_path = f"{memory_folder_path}/parties.json"

    parties = read_json_file(parties_file_path)

    assert parties["36898"] == {
        "id": "36898",
        "initials": "AVANTE",
        "name": "Avante",
    }
    assert parties["37905"] == {
        "id": "37905",
        "initials": "CIDADANIA",
        "name": "Cidadania",
    }
    assert parties["37902"] == {
        "id": "37902",
        "initials": "DC",
        "name": "Democracia Cristã",
    }
