from unittest.mock import Mock

import dados_abertos
import pytest
from utils.read_json_file import read_json_file

from seed import seed_deputies


@pytest.fixture(autouse=True)
def mock_fetch_deputies(monkeypatch):
    mock_fetch_deputies = Mock()
    mock_response = Mock()

    monkeypatch.setattr(dados_abertos, "fetch_deputies", mock_fetch_deputies)

    mock_fetch_deputies.return_value = mock_response
    mock_response.text = """
        <deputados>
            <deputado>
                <ideCadastro>73701</ideCadastro>
                <codOrcamento>1310</codOrcamento>
                <condicao>Titular</condicao>
                <matricula>291</matricula>
                <idParlamentar>1637158</idParlamentar>
                <nome>BENEDITA SOUZA DA SILVA SAMPAIO</nome>
                <nomeParlamentar>Benedita da Silva </nomeParlamentar>
                <urlFoto>http://www.camara.gov.br/internet/deputado/bandep/73701.jpg</urlFoto>
                <sexo>feminino</sexo>
                <uf>RJ</uf>
                <partido>PT</partido>
                <gabinete>330</gabinete>
                <anexo>4</anexo>
                <fone>3215-5330</fone>
                <email>dep.beneditadasilva@camara.leg.br</email>
                <comissoes>
                    <titular />
                    <suplente />
                </comissoes>
            </deputado>
            <deputado>
                <ideCadastro>73696</ideCadastro>
                <codOrcamento>1880</codOrcamento>
                <condicao>Titular</condicao>
                <matricula>471</matricula>
                <idParlamentar>1637159</idParlamentar>
                <nome>ANGELA REGINA HEINZEN AMIN HELOU</nome>
                <nomeParlamentar>Angela Amin </nomeParlamentar>
                <urlFoto>http://www.camara.gov.br/internet/deputado/bandep/73696.jpg</urlFoto>
                <sexo>feminino</sexo>
                <uf>SC</uf>
                <partido>PP</partido>
                <gabinete>252</gabinete>
                <anexo>4</anexo>
                <fone>3215-5252</fone>
                <email>dep.angelaamin@camara.leg.br</email>
                <comissoes>
                    <titular />
                    <suplente />
                </comissoes>
            </deputado>
        </deputados>
    """

    return mock_fetch_deputies


def test_seed_deputies(mock_fetch_deputies: Mock, test_memory_folder_path: str):
    seed_deputies()

    mock_fetch_deputies.assert_called_once()

    deputies_file_path = f"{test_memory_folder_path}/deputies.json"
    deputies = read_json_file(deputies_file_path)

    assert deputies["73701"] == {
        "id": "73701",
        "name": "BENEDITA SOUZA DA SILVA SAMPAIO",
        "party": "PT",
    }
    assert deputies["73696"] == {
        "id": "73696",
        "name": "ANGELA REGINA HEINZEN AMIN HELOU",
        "party": "PP",
    }
