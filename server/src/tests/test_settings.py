import pytest
from pydantic import ValidationError
from settings import DEV_DEFAULT_MEMORY_PATH, Settings


def test_settings_default_memory_folder_path_error(monkeypatch):
    monkeypatch.delenv("MEMORY_FOLDER_PATH")

    with pytest.raises(ValidationError) as validation_error:
        Settings()

    assert validation_error.value.errors()[0]["msg"] == "must set memory_folder_path in TEST environment"


def test_settings_default_memory_folder_path_in_dev_env(monkeypatch):
    monkeypatch.delenv("MEMORY_FOLDER_PATH")
    monkeypatch.setenv("ENVIRONMENT", "DEV")

    settings = Settings()

    assert settings.memory_folder_path == DEV_DEFAULT_MEMORY_PATH
