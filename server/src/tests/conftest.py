from pathlib import Path
from unittest.mock import Mock

import pytest
from settings import Settings


@pytest.fixture(autouse=True)
def test_environment(monkeypatch):
    monkeypatch.setenv("ENVIRONMENT", "TEST")


@pytest.fixture(autouse=True)
def setup_memory_folder_path(tmp_path: Path, monkeypatch):
    tests_memory_folder_path = tmp_path / "memory_data"
    tests_memory_folder_path.mkdir()

    monkeypatch.setenv("MEMORY_FOLDER_PATH", str(tests_memory_folder_path))


@pytest.fixture(autouse=True)
def mock_pool(monkeypatch):
    mock_pool = Mock()
    mock_pool_instance = Mock()

    def call_direct(callback, args_list):
        for args in args_list:
            callback(args)

        return []

    monkeypatch.setattr("multiprocessing.pool.Pool", mock_pool)

    mock_pool.return_value = mock_pool_instance
    mock_pool_instance.map.side_effect = call_direct


@pytest.fixture
def test_memory_folder_path():
    return Settings().memory_folder_path
