from datetime import datetime
from typing import List, Tuple
from unittest.mock import Mock

import pytest
from domain.entities import DeputyEntity, DeputyVoteEntity, PartyOrientationEntity, VoteEntity, VotingEntity

from mappers.map_votings_xml_to_entities import MapVotingsXMLToEntities

mock_proposition = Mock()
mock_voting = VotingEntity(
    "APROVADA",
    datetime(2021, 8, 17),
    "20:50",
    "APROVAR PROJETO",
    mock_proposition,
)
mock_voting_with_no_party_orientations = VotingEntity(
    "REJEITADA",
    datetime(2014, 5, 21),
    "05:24",
    "APROVAR PROJETO",
    mock_proposition,
)


@pytest.mark.parametrize(
    "votings_xml, expected_output",
    [
        (
            """
                <proposicao>
                    <Votacoes>
                        <Votacao
                            Resumo="APROVADA"
                            Data="17/8/2021"
                            Hora="20:50"
                            ObjVotacao="APROVAR PROJETO"
                            codSessao="17136"
                        >
                            <orientacaoBancada>
                                <bancada Sigla="PSL" orientacao="Sim            " />
                                <bancada Sigla="PSDB" orientacao="Não            " />
                            </orientacaoBancada>
                            <votos>
                                <Deputado
                                    Nome="ABC"
                                    ideCadastro="141417"
                                    Partido="PL        "
                                    UF="RR"
                                    Voto="Não    "
                                />
                                <Deputado
                                    Nome="XYZ"
                                    ideCadastro="204468"
                                    Partido="Rede      "
                                    UF="RR" Voto="Sim   "
                                />
                                <Deputado
                                    Nome="123"
                                    ideCadastro="204479"
                                    Partido="PSL       "
                                    UF="RR"
                                    Voto="-"
                                />
                            </votos>
                        </Votacao>
                        <Votacao
                            Resumo="REJEITADA"
                            Data="21/5/2014"
                            Hora="05:24"
                            ObjVotacao="APROVAR PROJETO"
                            codSessao="17135"
                        >
                            <votos>
                                <Deputado
                                    Nome="ABC"
                                    ideCadastro="141417"
                                    Partido="PL        "
                                    UF="RR"
                                    Voto="Não    "
                                />
                            </votos>
                        </Votacao>
                    </Votacoes>
                </proposicao>
            """,
            [
                (
                    mock_voting,
                    [
                        PartyOrientationEntity("PSL", VoteEntity.YES, mock_voting),
                        PartyOrientationEntity("PSDB", VoteEntity.NO, mock_voting),
                    ],
                    [
                        DeputyVoteEntity(DeputyEntity("141417", "ABC", "PL"), VoteEntity.NO, mock_voting),
                        DeputyVoteEntity(DeputyEntity("204468", "XYZ", "Rede"), VoteEntity.YES, mock_voting),
                        DeputyVoteEntity(DeputyEntity("204479", "123", "PSL"), VoteEntity.NULL, mock_voting),
                    ],
                ),
                (
                    mock_voting_with_no_party_orientations,
                    [],
                    [
                        DeputyVoteEntity(
                            DeputyEntity("141417", "ABC", "PL"),
                            VoteEntity.NO,
                            mock_voting_with_no_party_orientations,
                        ),
                    ],
                ),
            ],
        ),
    ],
)
def test_map_votings_xml_to_entities(
    votings_xml: str,
    expected_output: List[
        Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]],
    ],
):
    assert MapVotingsXMLToEntities(votings_xml, mock_proposition).map() == expected_output
