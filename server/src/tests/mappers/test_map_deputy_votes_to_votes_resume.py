from typing import List
from unittest.mock import Mock

import pytest
from domain.entities import DeputyVoteEntity, VoteEntity, VotesResumeEntity

from mappers import MapDeputyVotesToVotesResume


@pytest.mark.parametrize(
    "deputy_votes, vote_resume",
    [
        (
            [
                DeputyVoteEntity(Mock(), VoteEntity.YES, Mock()),
                DeputyVoteEntity(Mock(), VoteEntity.NO, Mock()),
                DeputyVoteEntity(Mock(), VoteEntity.YES, Mock()),
                DeputyVoteEntity(Mock(), VoteEntity.OBSTRUCT, Mock()),
                DeputyVoteEntity(Mock(), VoteEntity.NO, Mock()),
            ],
            VotesResumeEntity(yes=2, no=2, obstruct=1),
        ),
    ],
)
def test_map_deputy_votes_to_vote_resume(
    deputy_votes: List[DeputyVoteEntity],
    vote_resume: VotesResumeEntity,
):
    parsed_vote_resume = MapDeputyVotesToVotesResume(deputy_votes).map()

    assert parsed_vote_resume.free == vote_resume.free
    assert parsed_vote_resume.yes == vote_resume.yes
    assert parsed_vote_resume.no == vote_resume.no
    assert parsed_vote_resume.null == vote_resume.null
    assert parsed_vote_resume.obstruct == vote_resume.obstruct
