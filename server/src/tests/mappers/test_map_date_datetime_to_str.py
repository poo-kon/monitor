from datetime import datetime

import pytest

from mappers import MapDateDatetimeToStr


@pytest.mark.parametrize(
    "date, expected_date",
    [
        (datetime(2020, 12, 1), "01/12/2020"),
        (datetime(2005, 5, 9), "09/05/2005"),
        (None, ""),
    ],
)
def test_map_date_datetime_to_str(date: datetime, expected_date: str):
    assert MapDateDatetimeToStr(date).map() == expected_date
