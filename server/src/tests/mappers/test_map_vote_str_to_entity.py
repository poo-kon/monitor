import pytest
from domain.entities import VoteEntity

from mappers.map_vote_str_to_entity import MapVoteStrToEntity


@pytest.mark.parametrize(
    "vote, expected_vote",
    [
        ("Sim", VoteEntity.YES),
        ("Sim   ", VoteEntity.YES),
        ("Não", VoteEntity.NO),
        ("-", VoteEntity.NULL),
        ("Obstrução      ", VoteEntity.OBSTRUCT),
        ("Liberado       ", VoteEntity.FREE),
    ],
)
def test_map_vote_to_entity(vote: str, expected_vote: VoteEntity):
    assert MapVoteStrToEntity(vote).map() == expected_vote
