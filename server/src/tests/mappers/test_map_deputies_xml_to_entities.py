from domain.entities import DeputyEntity

from mappers import MapDeputiesXMLToEntities


def test_map_deputies_xml_to_entities():
    deputies_xml = """
        <deputados>
            <deputado>
                <ideCadastro>12345</ideCadastro>
                <codOrcamento>67890</codOrcamento>
                <condicao>Titular</condicao>
                <matricula>000</matricula>
                <idParlamentar>1234567</idParlamentar>
                <nome>NOME</nome>
                <nomeParlamentar>NOME_PARLAMENTAR</nomeParlamentar>
                <urlFoto>URL_FOTO</urlFoto>
                <sexo>feminino</sexo>
                <uf>RJ</uf>
                <partido>XY</partido>
                <gabinete>000</gabinete>
                <anexo>4</anexo>
                <fone>0000-0000</fone>
                <email>EMAIL</email>
                <comissoes>
                    <titular />
                    <suplente />
                </comissoes>
            </deputado>
        </deputados>
    """
    expected_deputies = [
        DeputyEntity(
            id="12345",
            name="NOME",
            party="XY",
        ),
    ]

    assert MapDeputiesXMLToEntities(deputies_xml).map() == expected_deputies


def test_map_many_deputies():
    deputies_xml = """
        <deputados>
            <deputado>
                <ideCadastro>12345</ideCadastro>
                <codOrcamento>67890</codOrcamento>
                <condicao>Titular</condicao>
                <matricula>000</matricula>
                <idParlamentar>1234567</idParlamentar>
                <nome>NOME_01</nome>
                <nomeParlamentar>NOME_PARLAMENTAR_01</nomeParlamentar>
                <urlFoto>URL_FOTO</urlFoto>
                <sexo>feminino</sexo>
                <uf>RJ</uf>
                <partido>XY</partido>
                <gabinete>000</gabinete>
                <anexo>4</anexo>
                <fone>0000-0000</fone>
                <email>EMAIL</email>
            </deputado>
            <deputado>
                <ideCadastro>XYZAB</ideCadastro>
                <codOrcamento>12345</codOrcamento>
                <condicao>Titular</condicao>
                <matricula>000</matricula>
                <idParlamentar>1234567</idParlamentar>
                <nome>NOME_02</nome>
                <nomeParlamentar>NOME_PARLAMENTAR_02</nomeParlamentar>
                <urlFoto>URL_FOTO</urlFoto>
                <sexo>feminino</sexo>
                <uf>RJ</uf>
                <partido>AB</partido>
                <gabinete>000</gabinete>
                <anexo>4</anexo>
                <fone>0000-0000</fone>
                <email>EMAIL</email>
            </deputado>
        </deputados>
    """
    expected_deputies = [
        DeputyEntity(
            id="12345",
            name="NOME_01",
            party="XY",
        ),
        DeputyEntity(
            id="XYZAB",
            name="NOME_02",
            party="AB",
        ),
    ]

    assert MapDeputiesXMLToEntities(deputies_xml).map() == expected_deputies
