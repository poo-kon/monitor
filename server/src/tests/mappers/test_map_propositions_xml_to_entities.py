from datetime import datetime
from typing import List

import pytest
from domain.entities import PropositionEntity, VoteTypeEntity

from mappers import MapPropositionsXMLtoEntities


@pytest.mark.parametrize(
    "propositions_xml, propositions_list",
    [
        (
            """
                <proposicoes>
                    <proposicao>
                        <codProposicao>257161</codProposicao>
                        <nomeProposicao>PL 3729/2004</nomeProposicao>
                        <dataVotacao>12/05/2021</dataVotacao>
                    </proposicao>
                    <proposicao>
                        <codProposicao>18156</codProposicao>
                        <nomeProposicao>PL 2462/1991</nomeProposicao>
                        <dataVotacao>04/05/2021</dataVotacao>
                    </proposicao>
                </proposicoes>
            """,
            [
                PropositionEntity(257161, VoteTypeEntity.PL, 3729, 2004, datetime(2021, 5, 12)),
                PropositionEntity(18156, VoteTypeEntity.PL, 2462, 1991, datetime(2021, 5, 4)),
            ],
        ),
        (
            """
                <proposicoes>
                    <proposicao>
                        <codProposicao>18156</codProposicao>
                        <nomeProposicao>PEC 211/2002 => PDC 1621/2002</nomeProposicao>
                        <dataVotacao>04/05/2021</dataVotacao>
                    </proposicao>
                </proposicoes>
            """,
            [
                PropositionEntity(18156, VoteTypeEntity.PEC, 211, 2002, datetime(2021, 5, 4)),
            ],
        ),
        (
            """
                <proposicoes>
                    <proposicao>
                        <codProposicao>18156</codProposicao>
                        <nomeProposicao>PEC 211/2002 => PDC 1621/2002</nomeProposicao>
                        <dataVotacao>04/05/2021</dataVotacao>
                    </proposicao>
                    <proposicao>
                        <codProposicao>18156</codProposicao>
                        <nomeProposicao>PEC 211/2002 => PDC 1621/2002</nomeProposicao>
                        <dataVotacao>04/05/2021</dataVotacao>
                    </proposicao>
                </proposicoes>
            """,
            [
                PropositionEntity(18156, VoteTypeEntity.PEC, 211, 2002, datetime(2021, 5, 4)),
            ],
        ),
        (
            """
                <proposicoes></proposicoes>
            """,
            [],
        ),
    ],
)
def test_map_propositions_xml_to_entities(
    propositions_xml: List[PropositionEntity],
    propositions_list: List[PropositionEntity],
):
    assert MapPropositionsXMLtoEntities(propositions_xml).map() == propositions_list
