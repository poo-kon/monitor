from unittest.mock import Mock

import pytest
from domain.entities import PropositionDetailsEntity

from mappers import MapPropositionDetailsXMLToEntities

mock_proposition = Mock()


@pytest.mark.parametrize(
    "propositions_details_xml, expected_propositions_details",
    [
        (
            """
                <proposicao tipo="PL " numero="999" ano="9999">
                    <tipoProposicao>Projeto de Lei</tipoProposicao>
                    <tema>Política, Partidos e Eleições</tema>
                    <Ementa>Altera Lei</Ementa>
                    <Autor>Senado Federal - Autor</Autor>
                    <partidoAutor>PSD </partidoAutor>
                </proposicao>
            """,
            PropositionDetailsEntity(
                proposition_code=mock_proposition.code,
                description="Altera Lei",
                theme="Política, Partidos e Eleições",
                author="Senado Federal - Autor",
                party_author="PSD",
            ),
        ),
        (
            """
                <proposicao tipo="PL " numero="222" ano="2222">
                    <nomeProposicao>PL 3962/2008</nomeProposicao>
                    <idProposicao>99999</idProposicao>
                    <idProposicaoPrincipal> </idProposicaoPrincipal>
                    <nomeProposicaoOrigem> </nomeProposicaoOrigem>
                    <tipoProposicao>Projeto de Lei</tipoProposicao>
                    <tema>Administração Pública; Previdência e Assistência Social</tema>
                    <Ementa>Projeto do governo</Ementa>
                    <ExplicacaoEmenta> </ExplicacaoEmenta>
                    <Autor>Poder Executivo</Autor>
                    <ideCadastro> </ideCadastro>
                    <ufAutor> </ufAutor>
                    <partidoAutor> </partidoAutor>
                    <DataApresentacao>29/08/2008</DataApresentacao>
                    <RegimeTramitacao>Urgência</RegimeTramitacao>
                    <UltimoDespacho Data="03/09/2008"></UltimoDespacho>
                    <Apreciacao></Apreciacao>
                    <Indexacao></Indexacao>
                    <Situacao></Situacao>
                    <LinkInteiroTeor></LinkInteiroTeor>
                    <apensadas/>
                </proposicao>
            """,
            PropositionDetailsEntity(
                proposition_code=mock_proposition.code,
                author="Poder Executivo",
                description="Projeto do governo",
                party_author="",
                theme="Administração Pública; Previdência e Assistência Social",
            ),
        ),
    ],
)
def test_map_proposition_details_xml_to_entities(
    propositions_details_xml: str,
    expected_propositions_details: PropositionDetailsEntity,
):
    assert (
        MapPropositionDetailsXMLToEntities(
            propositions_details_xml,
            mock_proposition,
        ).map()
        == expected_propositions_details
    )
