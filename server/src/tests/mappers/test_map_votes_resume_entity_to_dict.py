import pytest
from domain.entities import VotesResumeEntity

from mappers import MapVotesResumeEntityToDict


@pytest.mark.parametrize(
    "votes_resume, expected_dict",
    [
        (
            VotesResumeEntity(
                yes=5,
                free=5,
            ),
            {
                "yes": 5,
                "free": 5,
                "no": 0,
                "obstruct": 0,
                "null": 0,
            },
        ),
    ],
)
def test_map_votes_resume_entity_to_dict(
    votes_resume: VotesResumeEntity,
    expected_dict: dict,
):
    assert MapVotesResumeEntityToDict(votes_resume).map() == expected_dict
