import pytest

from mappers import MapDateStrToDatetime


@pytest.mark.parametrize(
    "date, expected_day, expected_month, expected_year",
    [
        ("11/10/2021", 11, 10, 2021),
        ("01/04/2006", 1, 4, 2006),
    ],
)
def test_map_date_str_to_datetime(
    date: str,
    expected_day: int,
    expected_month: int,
    expected_year: int,
):
    parsed_date = MapDateStrToDatetime(date).map()

    assert parsed_date.day == expected_day
    assert parsed_date.month == expected_month
    assert parsed_date.year == expected_year


@pytest.mark.parametrize(
    "date",
    [
        "",
        "02/02",
        "32/08/2011",
        "25/13/2021",
        "25/12^/2030",
    ],
)
def test_map_date_str_to_datetime_failures(date: str):
    assert MapDateStrToDatetime(date).map() is None
