from typing import Union

import pytest
from domain.entities import VoteTypeEntity

from mappers import MapVoteTypeStrToEntity


@pytest.mark.parametrize(
    "vote_type, expected",
    [
        ("PL", VoteTypeEntity.PL),
        ("PEC", VoteTypeEntity.PEC),
        ("ANY", None),
    ],
)
def test_map_vote_type_str_to_entity(vote_type: str, expected: Union[VoteTypeEntity, None]):
    assert MapVoteTypeStrToEntity(vote_type).map() == expected
