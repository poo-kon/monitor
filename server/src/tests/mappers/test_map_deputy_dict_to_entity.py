from domain.entities import DeputyEntity

from mappers.map_deputy_dict_to_entity import MapDeputyDictToEntity


def test_map_deputy_dict_to_entity():
    expected_entity = DeputyEntity(
        id="123",
        name="Deputy name",
        party="XYZ",
    )

    assert (
        MapDeputyDictToEntity(
            {
                "id": "123",
                "name": "Deputy name",
                "party": "XYZ",
            }
        ).map()
        == expected_entity
    )
