from unittest.mock import Mock

from domain.usecases import SeedDeputiesUseCase


def test_seed_deputies_usecase():
    mock_deputy_repository = Mock()
    mock_deputies = Mock()
    mock_deputy_repository.seed.return_value = mock_deputies
    usecase = SeedDeputiesUseCase(mock_deputy_repository)
    usecase.run()

    mock_deputy_repository.seed.assert_called()
    mock_deputy_repository.create_many.assert_called_with(mock_deputies)
