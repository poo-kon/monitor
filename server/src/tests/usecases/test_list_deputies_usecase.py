from unittest.mock import Mock

from domain.usecases import ListDeputiesUseCase


def test_list_deputies_usecase():
    mock_deputy_repository = Mock()
    usecase = ListDeputiesUseCase(mock_deputy_repository)

    usecase.run()
    mock_deputy_repository.list.assert_called()
