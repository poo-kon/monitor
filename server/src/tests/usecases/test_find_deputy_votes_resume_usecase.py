from datetime import datetime
from unittest.mock import Mock

from domain.entities import DeputyVoteEntity, PeriodEntity, VoteEntity
from domain.usecases import FindDeputyVotesResumeUseCase


def test_find_deputy_votes_resume_usecase():
    mock_deputy_repository = Mock()
    mock_deputy = Mock()
    mock_voting = Mock()
    mock_voting.date = "12/12/2000"

    mock_deputy_repository.list_votes.return_value = [
        DeputyVoteEntity(mock_deputy, VoteEntity.NULL, mock_voting),
        DeputyVoteEntity(mock_deputy, VoteEntity.YES, mock_voting),
        DeputyVoteEntity(mock_deputy, VoteEntity.NO, mock_voting),
        DeputyVoteEntity(mock_deputy, VoteEntity.YES, mock_voting),
        DeputyVoteEntity(mock_deputy, VoteEntity.NULL, mock_voting),
        DeputyVoteEntity(mock_deputy, VoteEntity.NULL, mock_voting),
    ]

    votes_resume = FindDeputyVotesResumeUseCase(mock_deputy_repository).run(
        mock_deputy,
        PeriodEntity(
            datetime(2000, 12, 12),
            datetime(2001, 12, 12),
        ),
    )

    assert votes_resume.yes == 2
    assert votes_resume.no == 1
    assert votes_resume.obstruct == 0
    assert votes_resume.free == 0
    assert votes_resume.null == 3
