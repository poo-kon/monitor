from unittest.mock import Mock

import pytest
from domain.usecases import FindDeputyUseCase


def test_find_deputy_usecase():
    deputy_repository = Mock()
    mock_deputy = Mock()
    mock_deputy_id = "123"

    deputy_repository.find.return_value = mock_deputy

    assert FindDeputyUseCase(deputy_repository).run(mock_deputy_id) == mock_deputy
    deputy_repository.find.assert_called_with(mock_deputy_id)


def test_not_found():
    deputy_repository = Mock()
    deputy_repository.find.return_value = None
    mock_deputy_id = "123"

    with pytest.raises(Exception):
        FindDeputyUseCase(deputy_repository).run(mock_deputy_id)
