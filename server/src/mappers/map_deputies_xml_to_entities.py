from typing import List, Tuple

import xmltodict
from domain.entities import DeputyEntity

from .map_deputy_dict_to_entity import MapDeputyDictToEntity


class MapDeputiesXMLToEntities:
    def __init__(self, xml_response: str):
        self.__xml__ = xml_response

    def map(self) -> List[DeputyEntity]:
        deputies_dict_list = self.__xml_to_dict__(self.__xml__)

        return list(
            map(
                self.__map_parsed_deputy_to_entity__,
                deputies_dict_list,
            )
        )

    def __xml_to_dict__(self, xml: str) -> dict:
        return xmltodict.parse(xml, force_list=True)["deputados"][0]["deputado"]

    def __map_parsed_deputy_to_entity__(self, deputy: List[Tuple]):
        parameters = {
            "ideCadastro": "id",
            "nome": "name",
            "partido": "party",
        }
        data = {
            "id": None,
            "name": "None",
            "party": None,
        }

        for key in deputy.keys():
            value = deputy[key]
            if parameters.get(key) is not None:
                data[parameters[key]] = value[0]

        return MapDeputyDictToEntity(data).map()
