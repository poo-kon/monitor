from typing import List, OrderedDict, Tuple

import xmltodict
from domain.entities import DeputyEntity, DeputyVoteEntity, PartyOrientationEntity, PropositionEntity, VotingEntity

from mappers import MapDateStrToDatetime, MapVoteStrToEntity


class MapVotingsXMLToEntities:
    def __init__(
        self,
        votings_xml: str,
        proposition: PropositionEntity,
    ):
        self.__votings_xml__ = votings_xml
        self.__proposition__ = proposition

    def map(self) -> List[Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]]]:
        votings_dict_list = self.__map_xml_to_dict__(self.__votings_xml__)

        return list(
            map(
                self.__map_votings_dict_to_entities__,
                votings_dict_list,
            ),
        )

    def __map_xml_to_dict__(self, xml: str) -> List[dict]:
        return xmltodict.parse(xml, force_list=True)["proposicao"][0]["Votacoes"][0]["Votacao"]

    def __map_votings_dict_to_entities__(self, votings: OrderedDict):
        voting = VotingEntity(
            resume=votings["@Resumo"],
            date=MapDateStrToDatetime(votings["@Data"]).map(),
            time=votings["@Hora"],
            objective=votings["@ObjVotacao"],
            proposition=self.__proposition__,
        )
        party_orientations = self.__map_party_orientations_dict_to_entity__(voting, votings)
        deputy_votes = list(
            map(
                lambda deputy_vote_dict: self.__map_deputy_vote_dict_to_dict__(
                    voting,
                    deputy_vote_dict,
                ),
                votings["votos"][0]["Deputado"],
            )
        )

        return (voting, party_orientations, deputy_votes)

    def __map_party_orientations_dict_to_entity__(
        self,
        voting: VotingEntity,
        votings: OrderedDict,
    ) -> List[PartyOrientationEntity]:
        try:
            party_orientations = list(
                map(
                    lambda party_orientation_dict: self.__map_party_orientation_dict_to_entity__(
                        voting,
                        party_orientation_dict,
                    ),
                    votings["orientacaoBancada"][0]["bancada"],
                )
            )

            return party_orientations
        except KeyError as error:
            if error.args[0] == "orientacaoBancada":
                return []

            raise error

    def __map_party_orientation_dict_to_entity__(
        self,
        voting: VotingEntity,
        orientation: OrderedDict,
    ) -> PartyOrientationEntity:
        return PartyOrientationEntity(
            party_name=orientation["@Sigla"],
            orientation=MapVoteStrToEntity(orientation["@orientacao"]).map(),
            voting=voting,
        )

    def __map_deputy_vote_dict_to_dict__(
        self,
        voting: VotingEntity,
        deputy_vote: OrderedDict,
    ):

        return DeputyVoteEntity(
            deputy=DeputyEntity(
                id=deputy_vote["@ideCadastro"],
                name=deputy_vote["@Nome"],
                party=deputy_vote["@Partido"].strip(),
            ),
            vote=MapVoteStrToEntity(deputy_vote["@Voto"]).map(),
            voting=voting,
        )
