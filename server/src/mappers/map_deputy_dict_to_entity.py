from domain.entities import DeputyEntity


class MapDeputyDictToEntity:
    def __init__(self, deputy: dict):
        self.__deputy__ = deputy

    def map(self):
        return DeputyEntity(
            id=self.__deputy__["id"],
            name=self.__deputy__["name"],
            party=self.__deputy__["party"],
        )
