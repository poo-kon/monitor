from typing import List

import xmltodict
from domain.entities import PropositionDetailsEntity, PropositionEntity


class MapPropositionDetailsXMLToEntities:
    def __init__(self, xml: str, proposition: PropositionEntity):
        self.__xml = xml
        self.__proposition = proposition

    def map(self) -> List[PropositionDetailsEntity]:
        proposition_details_dict = self.__xml_to_dict(self.__xml)

        return PropositionDetailsEntity(
            proposition_code=self.__proposition.code,
            author=proposition_details_dict["Autor"][0] or "",
            description=proposition_details_dict["Ementa"][0] or "",
            party_author=proposition_details_dict["partidoAutor"][0] or "",
            theme=proposition_details_dict["tema"][0] or "",
        )

    def __xml_to_dict(self, xml: str) -> dict:
        return xmltodict.parse(xml, force_list=True)["proposicao"][0]
