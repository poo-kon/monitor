from domain.entities import VotesResumeEntity


class MapVotesResumeEntityToDict:
    def __init__(self, votes_resume: VotesResumeEntity):
        self.__votes_resume__ = votes_resume

    def map(self) -> dict:
        return {
            "yes": self.__votes_resume__.yes,
            "no": self.__votes_resume__.no,
            "obstruct": self.__votes_resume__.obstruct,
            "free": self.__votes_resume__.free,
            "null": self.__votes_resume__.null,
        }
