from typing import Union

from domain.entities import VoteTypeEntity


class MapVoteTypeStrToEntity:
    def __init__(self, vote_type: str):
        self.__vote_type__ = vote_type

    def map(self) -> Union[VoteTypeEntity, None]:
        return {
            VoteTypeEntity.PEC.name: VoteTypeEntity.PEC,
            VoteTypeEntity.PL.name: VoteTypeEntity.PL,
        }.get(self.__vote_type__, None)
