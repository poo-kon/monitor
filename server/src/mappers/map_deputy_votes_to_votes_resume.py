from typing import List

from domain.entities import DeputyVoteEntity, VoteEntity, VotesResumeEntity


class MapDeputyVotesToVotesResume:
    def __init__(self, deputy_votes: List[DeputyVoteEntity]):
        self.__deputy_votes__ = deputy_votes

    def map(self) -> VotesResumeEntity:
        votes_count = {
            VoteEntity.YES: 0,
            VoteEntity.NO: 0,
            VoteEntity.OBSTRUCT: 0,
            VoteEntity.NULL: 0,
        }

        for deputy_vote in self.__deputy_votes__:
            votes_count[deputy_vote.vote] += 1

        return VotesResumeEntity(
            yes=votes_count[VoteEntity.YES],
            no=votes_count[VoteEntity.NO],
            obstruct=votes_count[VoteEntity.OBSTRUCT],
            null=votes_count[VoteEntity.NULL],
        )
