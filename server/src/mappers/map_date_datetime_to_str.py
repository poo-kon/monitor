from datetime import datetime


class MapDateDatetimeToStr:
    def __init__(self, date: datetime):
        self.__date = date

    def map(self) -> str:
        if self.__date is None:
            return ""

        return f"{self.__date.day:02}/{self.__date.month:02}/{self.__date.year}"
