import logging
from datetime import datetime

logger = logging.getLogger(__name__)


class MapDateStrToDatetime:
    def __init__(self, date: str):
        self.__date = date

    def map(self) -> datetime:
        try:
            day, month, year = self.__parse(self.__date)

            return datetime(
                day=int(day),
                month=int(month),
                year=int(year),
            )
        except Exception as exception:
            logging.info(exception)
            logging.info(f"failed to map date {self.__date} to datetime")

            return None

    def __parse(self, date: str):
        return date.split("/")
