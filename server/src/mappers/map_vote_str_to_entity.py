from domain.entities import VoteEntity


class MapVoteStrToEntity:
    def __init__(self, vote: str = ""):
        self.__vote__ = vote.strip()

    def map(self) -> VoteEntity:
        return {
            VoteEntity.YES.value: VoteEntity.YES,
            VoteEntity.NO.value: VoteEntity.NO,
            VoteEntity.FREE.value: VoteEntity.FREE,
            VoteEntity.OBSTRUCT.value: VoteEntity.OBSTRUCT,
        }.get(self.__vote__, VoteEntity.NULL)
