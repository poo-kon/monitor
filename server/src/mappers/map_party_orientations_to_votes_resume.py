from typing import List

from domain.entities import PartyOrientationEntity, VoteEntity, VotesResumeEntity


class MapPartyOrientationsToVotesResume:
    def __init__(self, party_orientations: List[PartyOrientationEntity]):
        self.__party_orientations__ = party_orientations

    def map(self) -> VotesResumeEntity:
        orientations_count = {
            VoteEntity.YES: 0,
            VoteEntity.NO: 0,
            VoteEntity.FREE: 0,
            VoteEntity.OBSTRUCT: 0,
            VoteEntity.NULL: 0,
        }

        for party_orientation in self.__party_orientations__:
            orientations_count[party_orientation.orientation] += 1

        return VotesResumeEntity(
            yes=orientations_count[VoteEntity.YES],
            no=orientations_count[VoteEntity.NO],
            obstruct=orientations_count[VoteEntity.OBSTRUCT],
            free=orientations_count[VoteEntity.FREE],
        )
