from typing import List, OrderedDict

import xmltodict
from domain.entities import PropositionEntity

from .map_date_str_to_datetime import MapDateStrToDatetime
from .map_vote_type_str_to_entity import MapVoteTypeStrToEntity


class MapPropositionsXMLtoEntities:
    def __init__(self, xml: str):
        self.__xml__ = xml

    def map(self) -> List[PropositionEntity]:
        propositions_dict = self.__xml_to_dict__(self.__xml__)

        if propositions_dict is None:
            return []

        return list(
            set(
                filter(
                    self.__not_none__,
                    list(
                        map(
                            self.__map_proposition_dict_to_entity__,
                            propositions_dict["proposicao"],
                        ),
                    ),
                ),
            ),
        )

    def __xml_to_dict__(self, xml: str) -> dict:
        return xmltodict.parse(xml, force_list=True)["proposicoes"][0]

    def __not_none__(self, propostion: PropositionEntity) -> bool:
        return propostion is not None

    def __map_proposition_dict_to_entity__(
        self,
        proposition: OrderedDict,
    ) -> PropositionEntity:
        parameters = {
            "codProposicao": "code",
            "nomeProposicao": "name",
            "dataVotacao": "vote_date",
        }
        data = {
            "code": None,
            "name": None,
            "vote_date": None,
        }

        for key in proposition.keys():
            value = proposition[key]
            if parameters.get(key) is not None:
                data[parameters[key]] = value[0]

        vote_type, number_year, *_ = data["name"].split(" ")
        number, year = number_year.split("/")

        vote_type_entity = MapVoteTypeStrToEntity(vote_type).map()

        return (
            PropositionEntity(
                code=int(data["code"]),
                type=vote_type_entity,
                number=int(number),
                year=int(year),
                vote_date=MapDateStrToDatetime(data["vote_date"]).map(),
            )
            if vote_type_entity is not None
            else None
        )
