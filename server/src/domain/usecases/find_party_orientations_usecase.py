from domain.entities import PartyEntity, PeriodEntity
from domain.repositories import PartyRepository


class FindPartyOrientationsUseCase:
    def __init__(self, party_repository: PartyRepository):
        self._party_repository = party_repository

    def run(
        self,
        party: PartyEntity,
        period: PeriodEntity = PeriodEntity(),
        keyword: str = "",
    ):
        return self._party_repository.list_orientations(
            party=party,
            period=period,
            keyword=keyword,
        )
