import logging
from multiprocessing import Pool
from typing import List

from domain.entities import PropositionEntity
from domain.repositories import PropositionRepository
from utils import current_year

logger = logging.getLogger(__name__)


class SeedPropositionsUseCase:
    def __init__(self, proposition_repository: PropositionRepository):
        self._proposition_repository = proposition_repository
        self._START_YEAR = 2000

    def run(self) -> List[PropositionEntity]:
        propositions = self._seed_propositions()
        self._seed_propositions_details(propositions)

        return propositions

    def _seed_propositions(self) -> List[PropositionEntity]:
        years = range(
            self._START_YEAR,
            current_year() + 1,
        )

        propositions: List[PropositionEntity] = []
        for year in years:
            propositions_from_year = self._proposition_repository.seed(year)
            propositions.extend(propositions_from_year)

        self._proposition_repository.save_propositions(propositions)

        return propositions

    def _seed_propositions_details(self, propositions: List[PropositionEntity]):
        pool = Pool(64)
        pool.map(self._seed_proposition_details, propositions)

    def _seed_proposition_details(self, proposition: PropositionEntity):
        proposition_details = self._proposition_repository.seed_details(proposition)

        if proposition_details is None:
            return

        self._proposition_repository.save_details(proposition, proposition_details)
        logging.info(f"Saved proposition {proposition.type.value} {proposition.number}/{proposition.year} details")
