from domain.repositories import DeputyRepository


class SeedDeputiesUseCase:
    def __init__(self, deputy_repository: DeputyRepository):
        self.__deputy_repository__ = deputy_repository

    def run(self):
        deputies = self.__deputy_repository__.seed()
        self.__deputy_repository__.create_many(deputies)
