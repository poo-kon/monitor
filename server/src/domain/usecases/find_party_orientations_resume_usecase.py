from domain.entities import PartyEntity, PeriodEntity, VotesResumeEntity
from domain.repositories import PartyRepository
from mappers import MapPartyOrientationsToVotesResume


class FindPartyOrientationsResumeUseCase:
    def __init__(self, party_repository: PartyRepository):
        self._party_repository = party_repository

    def run(
        self,
        party: PartyEntity,
        period: PeriodEntity = PeriodEntity(),
        keyword: str = "",
    ) -> VotesResumeEntity:
        party_orientations = self._party_repository.list_orientations(
            party=party,
            period=period,
            keyword=keyword,
        )

        return MapPartyOrientationsToVotesResume(party_orientations).map()
