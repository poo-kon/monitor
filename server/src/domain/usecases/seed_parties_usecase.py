from domain.repositories import PartyRepository


class SeedPartiesUseCase:
    def __init__(self, party_repository: PartyRepository):
        self.__party_repository__ = party_repository

    def run(self):
        parties = self.__party_repository__.seed()
        self.__party_repository__.create_many(parties)
