from domain.entities import PropositionDetailsEntity, PropositionEntity
from domain.repositories import PropositionRepository


class FindPropostionDetailsUseCase:
    def __init__(self, propostion_repository: PropositionRepository):
        self._proposition_repository = propostion_repository

    def run(self, propostion: PropositionEntity) -> PropositionDetailsEntity:
        return self._proposition_repository.find_details(propostion)
