from domain.entities import DeputyEntity
from domain.repositories import DeputyRepository


class FindDeputyUseCase:
    def __init__(self, deputy_repository: DeputyRepository):
        self.__deputy_repository__ = deputy_repository

    def run(self, deputy_id: str) -> DeputyEntity:
        deputy = self.__deputy_repository__.find(deputy_id)

        if deputy is None:
            raise Exception(f"deputy with id {deputy_id} does not exist")

        return deputy
