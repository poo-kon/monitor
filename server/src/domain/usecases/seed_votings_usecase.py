import logging
from multiprocessing import Pool
from typing import List, Tuple

from domain.entities import DeputyVoteEntity, PartyOrientationEntity, PropositionEntity, VotingEntity
from domain.repositories import PropositionRepository, VotingRepository

logger = logging.getLogger(__name__)


class SeedVotingUseCase:
    def __init__(
        self,
        voting_repository: VotingRepository,
        proposition_repository: PropositionRepository,
    ):
        self._voting_repository = voting_repository
        self._proposition_repository = proposition_repository

    def run(self) -> List[Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]]]:
        propositions = self._proposition_repository.list()

        pool = Pool(16)
        pool.map(self._seed_votes, propositions)

    def _seed_votes(self, proposition: PropositionEntity):
        votings: List[
            Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]]
        ] = self._voting_repository.seed_votings(proposition)
        self._voting_repository.save_votings(proposition, votings)

        logger.info(f"Saved proposition {proposition.type.value} {proposition.number}/{proposition.year} votes")
