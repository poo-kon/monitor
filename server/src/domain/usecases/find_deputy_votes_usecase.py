from typing import List

from domain.entities import DeputyEntity, DeputyVoteEntity, PeriodEntity
from domain.repositories import DeputyRepository


class FindDeputyVotesUseCase:
    def __init__(self, deputy_repository: DeputyRepository):
        self._deputy_repository = deputy_repository

    def run(
        self,
        deputy: DeputyEntity,
        period: PeriodEntity = PeriodEntity(),
        keyword: str = "",
    ) -> List[DeputyVoteEntity]:
        return self._deputy_repository.list_votes(
            deputy=deputy,
            period=period,
            keyword=keyword,
        )
