from typing import List

from domain.entities import PartyEntity
from domain.repositories import PartyRepository


class ListPartiesUseCase:
    def __init__(self, party_repository: PartyRepository):
        self.__party_repository__ = party_repository

    def run(self) -> List[PartyEntity]:
        return self.__party_repository__.list()
