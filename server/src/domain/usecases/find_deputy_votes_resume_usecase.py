from domain.entities import DeputyEntity, PeriodEntity, VotesResumeEntity
from domain.repositories import DeputyRepository
from mappers import MapDeputyVotesToVotesResume


class FindDeputyVotesResumeUseCase:
    def __init__(
        self,
        deputy_repository: DeputyRepository,
    ):
        self.__deputy_repository = deputy_repository

    def run(
        self,
        deputy: DeputyEntity,
        period: PeriodEntity = PeriodEntity(),
        keyword: str = "",
    ) -> VotesResumeEntity:
        votes = self.__deputy_repository.list_votes(
            deputy=deputy,
            period=period,
            keyword=keyword,
        )

        return MapDeputyVotesToVotesResume(votes).map()
