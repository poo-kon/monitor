from domain.entities import PartyEntity
from domain.repositories import PartyRepository


class FindPartyUseCase:
    def __init__(self, party_repository: PartyRepository):
        self.__party_repository__ = party_repository

    def run(self, party_id) -> PartyEntity:
        party = self.__party_repository__.find(party_id)

        if party is None:
            raise Exception(f"party with id {party_id} does not exist")

        return party
