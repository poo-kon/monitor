from typing import List

from domain.entities import DeputyEntity
from domain.repositories import DeputyRepository


class ListDeputiesUseCase:
    def __init__(self, deputy_repository: DeputyRepository):
        self.__deputy_repository__ = deputy_repository

    def run(self) -> List[DeputyEntity]:
        return self.__deputy_repository__.list()
