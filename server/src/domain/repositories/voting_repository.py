from abc import ABC, abstractmethod
from typing import List, Tuple

from domain.entities import DeputyVoteEntity, PartyOrientationEntity, PropositionEntity, VotingEntity


class VotingRepository(ABC):
    @abstractmethod
    def seed_votings(
        self,
        proposition: PropositionEntity,
    ) -> List[Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]]]:
        pass

    @abstractmethod
    def save_votings(
        self,
        proposition: PropositionEntity,
        votings: List[Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]]],
    ):
        pass
