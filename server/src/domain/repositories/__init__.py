from .deputy_repository import DeputyRepository  # noqa
from .party_repository import PartyRepository  # noqa
from .proposition_repository import PropositionRepository  # noqa
from .voting_repository import VotingRepository  # noqa
