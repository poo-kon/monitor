from abc import ABC, abstractmethod
from typing import List

from domain.entities import PropositionDetailsEntity, PropositionEntity


class PropositionRepository(ABC):
    @abstractmethod
    def seed(self, year: int) -> List[PropositionEntity]:
        pass

    @abstractmethod
    def save_propositions(self, propositions: List[PropositionEntity]):
        pass

    @abstractmethod
    def list(self) -> List[PropositionEntity]:
        pass

    @abstractmethod
    def seed_details(self, proposition: PropositionEntity) -> PropositionDetailsEntity:
        pass

    @abstractmethod
    def save_details(self, proposition: PropositionEntity, proposition_details: PropositionDetailsEntity):
        pass

    @abstractmethod
    def find_details(self, proposition: PropositionEntity) -> PropositionDetailsEntity:
        pass
