from abc import ABC, abstractmethod
from typing import List

from domain.entities import DeputyEntity, DeputyVoteEntity, PeriodEntity


class DeputyRepository(ABC):
    @abstractmethod
    def seed(self) -> List[DeputyEntity]:
        pass

    @abstractmethod
    def create_many(self, deputies: List[DeputyEntity]):
        pass

    @abstractmethod
    def list(self) -> List[DeputyEntity]:
        pass

    @abstractmethod
    def list_votes(self, deputy: DeputyEntity, period: PeriodEntity, keyword: str) -> List[DeputyVoteEntity]:
        pass

    @abstractmethod
    def find(self, deputy_id: str) -> DeputyEntity:
        pass
