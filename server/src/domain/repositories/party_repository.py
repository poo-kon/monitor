from abc import ABC, abstractmethod
from typing import List

from domain.entities import PartyEntity, PartyOrientationEntity, PeriodEntity


class PartyRepository(ABC):
    @abstractmethod
    def list(self) -> List[PartyEntity]:
        pass

    @abstractmethod
    def seed(self) -> List[PartyEntity]:
        pass

    @abstractmethod
    def create_many(self, parties: List[PartyEntity]):
        pass

    @abstractmethod
    def find(self, party_id: str) -> PartyEntity:
        pass

    @abstractmethod
    def list_orientations(
        self,
        party: PartyEntity,
        period: PeriodEntity,
        keyword: str,
    ) -> List[PartyOrientationEntity]:
        pass
