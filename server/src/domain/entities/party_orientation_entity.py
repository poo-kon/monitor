from dataclasses import dataclass

from .vote_entity import VoteEntity
from .voting_entity import VotingEntity


@dataclass
class PartyOrientationEntity:
    party_name: str
    orientation: VoteEntity
    voting: VotingEntity
