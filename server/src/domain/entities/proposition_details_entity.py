from dataclasses import dataclass


@dataclass
class PropositionDetailsEntity:
    proposition_code: int
    description: str
    theme: str = ""
    author: str = ""
    party_author: str = ""
