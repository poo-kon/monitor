from enum import Enum


class VoteEntity(Enum):
    YES = "Sim"
    NO = "Não"
    OBSTRUCT = "Obstrução"
    FREE = "Liberado"
    NULL = ""
