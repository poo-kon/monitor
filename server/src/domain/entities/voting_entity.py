from dataclasses import dataclass
from datetime import datetime

from .period_entity import PeriodEntity
from .proposition_entity import PropositionEntity


@dataclass
class VotingEntity:
    resume: str
    date: datetime
    time: str
    objective: str
    proposition: PropositionEntity

    def in_period(self, period: PeriodEntity) -> bool:
        return period.start_date <= self.date and self.date <= period.end_date
