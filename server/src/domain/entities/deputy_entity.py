from dataclasses import dataclass


@dataclass
class DeputyEntity:
    id: str
    name: str
    party: str
