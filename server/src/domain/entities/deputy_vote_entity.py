from dataclasses import dataclass

from .deputy_entity import DeputyEntity
from .vote_entity import VoteEntity
from .voting_entity import VotingEntity


@dataclass
class DeputyVoteEntity:
    deputy: DeputyEntity
    vote: VoteEntity
    voting: VotingEntity
