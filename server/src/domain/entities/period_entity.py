from datetime import datetime


class PeriodEntity:
    def __init__(
        self,
        start_date: datetime = datetime.min,
        end_date: datetime = datetime.max,
    ):
        self.start_date: datetime = start_date.replace(tzinfo=None)
        self.end_date: datetime = end_date.replace(tzinfo=None)
        self.__validate()

    def __validate(self):
        if self.start_date > self.end_date:
            raise ValueError(f"start_date({self.start_date}) > end_date({self.end_date})")
