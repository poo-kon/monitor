from dataclasses import dataclass


@dataclass
class PartyEntity:
    id: str
    name: str
    initials: str
