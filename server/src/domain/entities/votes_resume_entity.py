class VotesResumeEntity:
    def __init__(
        self,
        yes: int = 0,
        no: int = 0,
        obstruct: int = 0,
        free: int = 0,
        null: int = 0,
    ) -> None:
        self.yes = 0 if yes is None else yes
        self.no = 0 if no is None else no
        self.obstruct = 0 if obstruct is None else obstruct
        self.free = 0 if free is None else free
        self.null = 0 if null is None else null
