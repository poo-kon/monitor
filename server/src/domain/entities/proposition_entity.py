from dataclasses import dataclass
from datetime import datetime

from .vote_type_entity import VoteTypeEntity


@dataclass
class PropositionEntity:
    code: int
    type: VoteTypeEntity
    number: int
    year: int
    vote_date: datetime

    def __hash__(self) -> int:
        return self.code
