from enum import Enum


class VoteTypeEntity(Enum):
    PL = "PL"
    PEC = "PEC"

    @classmethod
    def types(cls):
        return [name for name, _ in VoteTypeEntity.__members__.items()]
