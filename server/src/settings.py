import pathlib
from enum import Enum

from pydantic import BaseSettings, validator


class Environment(Enum):
    TEST = "TEST"
    DEV = "DEV"


DEV_DEFAULT_MEMORY_PATH = f"{pathlib.Path(__file__).parent.resolve()}/memory_data"


class Settings(BaseSettings):
    environment: Environment = Environment.DEV
    memory_folder_path: str = DEV_DEFAULT_MEMORY_PATH
    web_url = "http://localhost:3000"

    @validator("memory_folder_path")
    def default_memory_folder_path(cls, memory_folder_path, values):
        environment = values["environment"]

        if environment == Environment.TEST and memory_folder_path == DEV_DEFAULT_MEMORY_PATH:
            raise ValueError("must set memory_folder_path in TEST environment")

        return memory_folder_path
