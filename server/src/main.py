from datetime import datetime
from typing import List, Union

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

from domain.entities import (
    DeputyVoteEntity,
    PartyOrientationEntity,
    PeriodEntity,
    PropositionDetailsEntity,
    PropositionEntity,
)
from domain.usecases import (
    FindDeputyUseCase,
    FindDeputyVotesResumeUseCase,
    FindDeputyVotesUseCase,
    FindPartyOrientationsResumeUseCase,
    FindPartyOrientationsUseCase,
    FindPartyUseCase,
    FindPropostionDetailsUseCase,
    ListDeputiesUseCase,
    ListPartiesUseCase,
)
from mappers import MapVotesResumeEntityToDict
from services import ServiceDeputyRepository, ServicePartyRepository, ServicePropositionRepository
from settings import Settings

settings = Settings()

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[settings.web_url],
    allow_methods=["*"],
)

service_deputy_repository = ServiceDeputyRepository()
service_proposition_repository = ServicePropositionRepository()
service_party_repository = ServicePartyRepository()

find_deputy_usecase = FindDeputyUseCase(service_deputy_repository)
find_deputy_votes_usecase = FindDeputyVotesUseCase(service_deputy_repository)
find_deputy_votes_resume_usecase = FindDeputyVotesResumeUseCase(service_deputy_repository)
find_proposition_details_usecase = FindPropostionDetailsUseCase(service_proposition_repository)
find_party_usecase = FindPartyUseCase(service_party_repository)
find_party_orientations_usecase = FindPartyOrientationsUseCase(service_party_repository)
find_party_orientations_resume_usecase = FindPartyOrientationsResumeUseCase(service_party_repository)


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/parties")
def get_parties():
    parties = ListPartiesUseCase(ServicePartyRepository()).run()
    return {"parties": parties}


@app.get("/deputies")
def get_deputies():
    deputies = ListDeputiesUseCase(ServiceDeputyRepository()).run()
    return {"deputies": deputies}


class VotesResumeRequest(BaseModel):
    party_ids: List[str]
    deputy_ids: List[str]
    start: datetime
    end: datetime


@app.post("/votes-resume")
def get_votes_resume(request: VotesResumeRequest):
    period = PeriodEntity(request.start, request.end)

    return {
        "deputy_votes_resumes": __find_deputy_votes_resumes__(
            request.deputy_ids,
            period,
        ),
        "party_orientations_resumes": __find_party_orientations_resumes__(
            request.party_ids,
            period,
        ),
    }


def __find_deputy_votes_resumes__(
    deputy_ids: List[str],
    period: PeriodEntity,
) -> dict:
    deputy_votes_resumes_dict = {}

    for deputy_id in deputy_ids:
        deputy_votes_resumes_dict[deputy_id] = MapVotesResumeEntityToDict(
            find_deputy_votes_resume_usecase.run(
                find_deputy_usecase.run(
                    deputy_id,
                ),
                period,
            )
        ).map()

    return deputy_votes_resumes_dict


def __find_party_orientations_resumes__(
    party_ids: List[str],
    period: PeriodEntity,
) -> dict:
    party_orientations_resumes_dict = {}

    for party_id in party_ids:
        party_orientations_resumes_dict[party_id] = MapVotesResumeEntityToDict(
            find_party_orientations_resume_usecase.run(
                party=find_party_usecase.run(
                    party_id,
                ),
                period=period,
            )
        ).map()

    return party_orientations_resumes_dict


class ActorsVotesRequest(BaseModel):
    actor_id: str
    actor_type: str
    start_date: datetime
    end_date: datetime


@app.post("/actors-votes")
def get_actors_votes(request: ActorsVotesRequest):
    period = PeriodEntity(
        start_date=request.start_date,
        end_date=request.end_date,
    )
    votes = []
    propositions_votings = {}
    is_deputy = request.actor_type == "deputy"

    if is_deputy:
        deputy = find_deputy_usecase.run(request.actor_id)
        votes = find_deputy_votes_usecase.run(
            deputy=deputy,
            period=period,
        )
    else:
        party = find_party_usecase.run(request.actor_id)
        votes = find_party_orientations_usecase.run(
            party=party,
            period=period,
        )

    for vote in votes:
        code = vote.voting.proposition.code

        if propositions_votings.get(code) is None:
            proposition_details = find_proposition_details_usecase.run(vote.voting.proposition)
            propositions_votings[code] = {
                "votes": [],
                "proposition": vote.voting.proposition,
                "details": proposition_details,
            }

        propositions_votings[code]["votes"].append(vote)

    return {
        "propositions_votings": list(
            map(
                lambda proposition_code: _map_proposition_voting(
                    propositions_votings[proposition_code],
                ),
                propositions_votings.keys(),
            ),
        ),
    }


def _map_proposition_voting(proposition_voting: dict) -> dict:
    proposition: PropositionEntity = proposition_voting["proposition"]
    proposition_details: PropositionDetailsEntity = proposition_voting["details"]
    votes: List[Union[DeputyVoteEntity, PartyOrientationEntity]] = proposition_voting["votes"]

    return {
        "proposition": {
            "code": proposition.code,
            "type": proposition.type.value,
            "number": proposition.number,
            "year": proposition.year,
            "description": proposition_details.description,
            "theme": proposition_details.theme,
        },
        "votings": list(
            map(
                lambda vote: {
                    "resume": vote.voting.resume,
                    "objective": vote.voting.objective,
                    "date": vote.voting.date,
                    "vote": vote.vote.value if isinstance(vote, DeputyVoteEntity) else vote.orientation.value,
                },
                votes,
            ),
        ),
    }
