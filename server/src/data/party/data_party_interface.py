from abc import ABC, abstractmethod
from typing import List

from domain.entities import PartyEntity


class DataPartyInterface(ABC):
    @abstractmethod
    def save_many(self, parties: List[PartyEntity]):
        pass

    @abstractmethod
    def find_all(self) -> List[PartyEntity]:
        pass

    @abstractmethod
    def find(self, party_id: str) -> PartyEntity:
        pass
