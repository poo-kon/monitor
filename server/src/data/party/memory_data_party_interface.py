import json
from typing import List

from domain.entities import PartyEntity
from settings import Settings
from utils import read_json_file

from .data_party_interface import DataPartyInterface


class MemoryDataPartyInterface(DataPartyInterface):
    def __init__(self):
        settings = Settings()
        self.__parties_path__ = f"{settings.memory_folder_path}/parties.json"

    def save_many(self, parties: List[PartyEntity]):
        parties_json = {}
        for party in parties:
            parties_json[party.id] = self.__map_party_entity_to_dict__(party)

        with open(self.__parties_path__, "w") as parties_file:
            json.dump(
                parties_json,
                parties_file,
                sort_keys=True,
                indent=2,
                ensure_ascii=False,
            )

    def find_all(self) -> List[PartyEntity]:
        with open(self.__parties_path__, "r") as parties_file:
            parties_json = json.load(parties_file)

            parties = []
            for id in parties_json.keys():
                parties.append(
                    self.__map_party_dict_to_entity__(
                        parties_json[id],
                    )
                )

            return parties

    def find(self, party_id: str) -> PartyEntity:
        parties_json = read_json_file(self.__parties_path__)
        party_json = parties_json.get(party_id, None)

        return self.__map_party_dict_to_entity__(party_json) if party_json is not None else None

    def __map_party_entity_to_dict__(self, party: PartyEntity) -> dict:
        return {
            "id": party.id,
            "name": party.name,
            "initials": party.initials,
        }

    def __map_party_dict_to_entity__(self, party: dict) -> PartyEntity:
        return PartyEntity(
            id=party["id"],
            name=party["name"],
            initials=party["initials"],
        )
