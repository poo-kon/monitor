import re
from os import listdir
from os.path import isfile, join
from pathlib import Path
from typing import List, Tuple

from data.deputy import memory_map_deputy_dict_to_entity, memory_map_deputy_entity_to_dict
from data.proposition import memory_map_proposition_dict_to_entity, memory_map_proposition_entity_to_dict
from domain.entities import (
    DeputyEntity,
    DeputyVoteEntity,
    PartyEntity,
    PartyOrientationEntity,
    PeriodEntity,
    PropositionDetailsEntity,
    PropositionEntity,
    VotingEntity,
)
from mappers import MapDateDatetimeToStr, MapDateStrToDatetime, MapVoteStrToEntity
from settings import Settings
from utils import list_files_from_folder, read_json_file, write_json_file

from .data_voting_interface import DataVotingInterface


class MemoryDataVotingInterface(DataVotingInterface):
    def __init__(self):
        settings = Settings()
        self.__votings_folder_path__ = f"{settings.memory_folder_path}/votings"
        Path(self.__votings_folder_path__).mkdir(parents=True, exist_ok=True)

        self.__propositions_details_folder_path = f"{settings.memory_folder_path}/propositions_details"

    def save_many(
        self,
        proposition: PropositionEntity,
        votings: List[Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]]],
    ):
        votings_from_proposition = []

        for voting in votings:
            voting_dict = self.__map_voting_entity_to_dict__(voting[0])

            party_orientations_dict = {}
            for party_orientation in voting[1]:
                party_orientations_dict[party_orientation.party_name] = self.__map_party_orientation_entity_to_dict__(
                    party_orientation,
                )

            deputy_votes_dict = {}
            for deputy_vote in voting[2]:
                deputy_votes_dict[deputy_vote.deputy.id] = self.__map_deputy_vote_entity_to_dict__(
                    deputy_vote,
                )

            votings_from_proposition.append(
                {
                    "voting": voting_dict,
                    "party_orientations": party_orientations_dict,
                    "deputy_votes": deputy_votes_dict,
                }
            )

        self.__write_votings__(
            proposition,
            {
                "votings": votings_from_proposition,
            },
        )

    def __write_votings__(self, proposition: PropositionEntity, votings: dict):
        write_json_file(f"{self.__votings_folder_path__}/{proposition.code}.json", votings)

    def __map_voting_entity_to_dict__(self, voting: VotingEntity) -> dict:
        return {
            "resume": voting.resume,
            "date": MapDateDatetimeToStr(voting.date).map(),
            "time": voting.time,
            "objective": voting.objective,
            "proposition": memory_map_proposition_entity_to_dict(voting.proposition),
        }

    def __map_party_orientation_entity_to_dict__(self, party_orientation: PartyOrientationEntity) -> dict:
        return {
            "party_name": party_orientation.party_name,
            "orientation": party_orientation.orientation.value,
        }

    def __map_deputy_vote_entity_to_dict__(self, deputy_vote: DeputyVoteEntity) -> dict:
        return {
            "deputy": memory_map_deputy_entity_to_dict(deputy_vote.deputy),
            "vote": deputy_vote.vote.value,
        }

    def find_deputy_votes(
        self,
        deputy: DeputyEntity,
        keyword: str = "",
        period: PeriodEntity = PeriodEntity(),
    ) -> List[DeputyVoteEntity]:
        propositions_codes = self.__filter_propositions(keyword)
        deputy_votes = self.__filter_deputy_votes(propositions_codes, deputy, period)

        return deputy_votes

    def __get_voting_file_paths__(self) -> List[str]:
        return [
            f"{self.__votings_folder_path__}/{file}"
            for file in listdir(self.__votings_folder_path__)
            if isfile(
                join(self.__votings_folder_path__, file),
            )
        ]

    def __map_deputy_vote_dict_to_entity__(
        self,
        deputy_vote: dict,
        voting: VotingEntity,
    ) -> DeputyVoteEntity:
        return DeputyVoteEntity(
            deputy=memory_map_deputy_dict_to_entity(deputy_vote["deputy"]),
            vote=MapVoteStrToEntity(deputy_vote["vote"]).map(),
            voting=voting,
        )

    def __map_voting_dict_to_entity__(self, voting: dict) -> VotingEntity:
        return VotingEntity(
            resume=voting["resume"],
            date=MapDateStrToDatetime(voting["date"]).map(),
            time=voting["time"],
            objective=voting["objective"],
            proposition=memory_map_proposition_dict_to_entity(voting["proposition"]),
        )

    def find_party_orientations(
        self,
        party: PartyEntity,
        period: PeriodEntity = PeriodEntity(),
        keyword: str = "",
    ) -> List[PartyOrientationEntity]:
        propositions_codes = self.__filter_propositions(keyword)
        party_orientations = self._filter_party_orientations(propositions_codes, party, period)

        return party_orientations

    def __map_party_orientation_dict_to_entity__(
        self,
        party_orientation: dict,
        voting: VotingEntity,
    ) -> PartyOrientationEntity:
        return PartyOrientationEntity(
            party_name=party_orientation["party_name"],
            orientation=MapVoteStrToEntity(party_orientation["orientation"]).map(),
            voting=voting,
        )

    def __map_proposition_details_json_to_entity(self, proposition_details: dict) -> PropositionDetailsEntity:
        return PropositionDetailsEntity(
            proposition_code=proposition_details["proposition_code"],
            description=proposition_details["description"],
            author=proposition_details["author"],
            party_author=proposition_details["party_author"],
            theme=proposition_details["theme"],
        )

    def __filter_propositions(self, keyword: str = "") -> List[str]:
        propositions_details_files = list_files_from_folder(self.__propositions_details_folder_path)

        filtered_propositions_codes: List[str] = []
        for proposition_details_file in propositions_details_files:
            proposition_details_json = read_json_file(proposition_details_file)
            proposition_details = self.__map_proposition_details_json_to_entity(proposition_details_json)
            proposition_matches_keyword = (
                re.search(keyword, proposition_details.description) is not None
                or re.search(keyword, proposition_details.theme) is not None
            )

            if proposition_matches_keyword:
                filtered_propositions_codes.append(proposition_details.proposition_code)

        return filtered_propositions_codes

    def __filter_deputy_votes(
        self, propositions_codes: List[str], deputy: DeputyEntity, period: PeriodEntity
    ) -> List[DeputyVoteEntity]:
        filtered_deputy_votes: List[DeputyVoteEntity] = []

        for proposition_code in propositions_codes:
            proposition_votings_file_path = join(self.__votings_folder_path__, f"{proposition_code}.json")
            proposition_votings_json = read_json_file(proposition_votings_file_path)

            for voting_json in proposition_votings_json["votings"]:
                voting = self.__map_voting_dict_to_entity__(voting_json["voting"])

                if not voting.in_period(period):
                    continue

                deputy_vote_dict = voting_json["deputy_votes"].get(deputy.id, None)

                if deputy_vote_dict is not None:
                    filtered_deputy_votes.append(
                        self.__map_deputy_vote_dict_to_entity__(
                            deputy_vote_dict,
                            voting,
                        )
                    )

        return filtered_deputy_votes

    def _filter_party_orientations(
        self,
        propositions_codes: List[str],
        party: PartyEntity,
        period: PeriodEntity,
    ):
        filtered_party_orientations: List[PartyOrientationEntity] = []

        print(len(propositions_codes))

        for proposition_code in propositions_codes:
            proposition_votings_file_path = join(self.__votings_folder_path__, f"{proposition_code}.json")
            proposition_votings_json = read_json_file(proposition_votings_file_path)

            for voting_json in proposition_votings_json["votings"]:
                voting = self.__map_voting_dict_to_entity__(voting_json["voting"])

                if not voting.in_period(period):
                    continue

                party_orientation_dict = voting_json["party_orientations"].get(party.initials, None)

                if party_orientation_dict is not None:
                    filtered_party_orientations.append(
                        self.__map_party_orientation_dict_to_entity__(
                            party_orientation_dict,
                            voting,
                        )
                    )

        return filtered_party_orientations
