from abc import ABC, abstractmethod
from typing import List, Tuple

from domain.entities import (
    DeputyEntity,
    DeputyVoteEntity,
    PartyEntity,
    PartyOrientationEntity,
    PeriodEntity,
    PropositionEntity,
    VotingEntity,
)


class DataVotingInterface(ABC):
    @abstractmethod
    def save_many(
        self,
        proposition: PropositionEntity,
        votings: List[Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]]],
    ):
        pass

    @abstractmethod
    def find_deputy_votes(self, deputy: DeputyEntity) -> List[DeputyVoteEntity]:
        pass

    @abstractmethod
    def find_party_orientations(
        self,
        party: PartyEntity,
        period: PeriodEntity,
        keyword: str,
    ) -> List[PartyOrientationEntity]:
        pass
