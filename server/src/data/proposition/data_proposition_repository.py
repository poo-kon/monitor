from abc import abstractmethod
from typing import List

from domain.entities import PropositionDetailsEntity, PropositionEntity


class DataPropositionRepository:
    @abstractmethod
    def save_propositions(self, propositions: List[PropositionEntity]):
        pass

    @abstractmethod
    def find_all(self) -> List[PropositionEntity]:
        pass

    @abstractmethod
    def save_details(self, proposition: PropositionEntity, proposition_details: PropositionDetailsEntity):
        pass

    @abstractmethod
    def find_details(self, proposition: PropositionEntity) -> PropositionDetailsEntity:
        pass
