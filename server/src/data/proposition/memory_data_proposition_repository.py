import json
from typing import List

from domain.entities import PropositionDetailsEntity, PropositionEntity
from settings import Settings
from utils import create_folder_if_not_exists, read_json_file, write_json_file

from .data_proposition_repository import DataPropositionRepository
from .memory_map_proposition_dict_to_entity import memory_map_proposition_dict_to_entity
from .memory_map_proposition_entity_to_dict import memory_map_proposition_entity_to_dict


class MemoryDataPropositionRepository(DataPropositionRepository):
    def __init__(self):
        settings = Settings()
        self.__propositions_path__ = f"{settings.memory_folder_path}/propositions.json"

        self.__propositions_details_folder = f"{settings.memory_folder_path}/propositions_details"
        create_folder_if_not_exists(self.__propositions_details_folder)

    def save_propositions(self, propositions: List[PropositionEntity]):
        propositions_json = {}
        for proposition in propositions:
            propositions_json[proposition.code] = memory_map_proposition_entity_to_dict(proposition)

        with open(self.__propositions_path__, "w") as propositions_file:
            json.dump(
                propositions_json,
                propositions_file,
                sort_keys=True,
                indent=2,
                ensure_ascii=False,
            )

    def find_all(self) -> List[PropositionEntity]:
        with open(self.__propositions_path__, "r") as propositions_file:
            propositions_json = json.load(propositions_file)

            propositions = []
            for code in propositions_json.keys():
                propositions.append(
                    memory_map_proposition_dict_to_entity(
                        propositions_json[code],
                    )
                )

            return propositions

    def save_details(self, proposition: PropositionEntity, proposition_details: PropositionDetailsEntity):
        write_json_file(
            path=self.__proposition_details_path(proposition),
            data_json=self.__map_proposition_details_to_json(proposition_details),
        )

    def __proposition_details_path(self, proposition: PropositionEntity) -> str:
        return f"{self.__propositions_details_folder}/{proposition.code}.json"

    def __map_proposition_details_to_json(
        self,
        proposition_details: PropositionDetailsEntity,
    ) -> dict:
        return {
            "proposition_code": proposition_details.proposition_code,
            "description": proposition_details.description,
            "theme": proposition_details.theme,
            "author": proposition_details.author,
            "party_author": proposition_details.party_author,
        }

    def find_details(self, proposition: PropositionEntity) -> PropositionDetailsEntity:
        proposition_details_json = read_json_file(self.__proposition_details_path(proposition))

        return self._map_proposition_details_json_to_entity(proposition_details_json)

    def _map_proposition_details_json_to_entity(self, proposition_details_json: dict) -> PropositionDetailsEntity:
        return PropositionDetailsEntity(
            author=proposition_details_json["author"],
            description=proposition_details_json["description"],
            theme=proposition_details_json["theme"],
            party_author=proposition_details_json["party_author"],
            proposition_code=proposition_details_json["proposition_code"],
        )
