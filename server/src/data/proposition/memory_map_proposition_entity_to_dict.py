from domain.entities import PropositionEntity
from mappers import MapDateDatetimeToStr


def memory_map_proposition_entity_to_dict(proposition: PropositionEntity) -> dict:
    return {
        "code": proposition.code,
        "type": proposition.type.value,
        "number": proposition.number,
        "year": proposition.year,
        "vote_date": MapDateDatetimeToStr(proposition.vote_date).map(),
    }
