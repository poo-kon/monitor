from domain.entities import PropositionEntity
from mappers import MapDateStrToDatetime, MapVoteTypeStrToEntity


def memory_map_proposition_dict_to_entity(proposition: dict) -> PropositionEntity:
    return PropositionEntity(
        code=proposition["code"],
        number=proposition["number"],
        year=proposition["year"],
        type=MapVoteTypeStrToEntity(proposition["type"]).map(),
        vote_date=MapDateStrToDatetime(proposition["vote_date"]).map(),
    )
