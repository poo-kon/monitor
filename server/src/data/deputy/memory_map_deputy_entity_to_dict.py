from domain.entities import DeputyEntity


def memory_map_deputy_entity_to_dict(deputy: DeputyEntity) -> dict:
    return {
        "id": deputy.id,
        "name": deputy.name,
        "party": deputy.party,
    }
