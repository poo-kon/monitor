import json
from typing import List

from domain.entities import DeputyEntity
from settings import Settings
from utils import read_json_file

from .data_deputy_interface import DataDeputyInterface
from .memory_map_deputy_dict_to_entity import memory_map_deputy_dict_to_entity
from .memory_map_deputy_entity_to_dict import memory_map_deputy_entity_to_dict


class MemoryDataDeputyInterface(DataDeputyInterface):
    def __init__(self):
        settings = Settings()
        self.__deputies_path__ = f"{settings.memory_folder_path}/deputies.json"

    def save_many(self, deputies: List[DeputyEntity]):
        deputies_json = {}
        for deputy in deputies:
            deputies_json[deputy.id] = memory_map_deputy_entity_to_dict(deputy)

        with open(self.__deputies_path__, "w") as deputies_file:
            json.dump(
                deputies_json,
                deputies_file,
                sort_keys="True",
                indent=2,
                ensure_ascii=False,
            )

    def find_all(self) -> List[DeputyEntity]:
        with open(self.__deputies_path__, "r") as deputies_file:
            deputies_json = json.load(deputies_file)

            deputies = []
            for id in deputies_json.keys():
                deputies.append(
                    memory_map_deputy_dict_to_entity(
                        deputies_json[id],
                    ),
                )

            return deputies

    def find(self, deputy_id: str) -> DeputyEntity:
        deputies_json = read_json_file(self.__deputies_path__)
        deputy_json = deputies_json.get(deputy_id, None)

        return memory_map_deputy_dict_to_entity(deputy_json) if deputy_json is not None else None
