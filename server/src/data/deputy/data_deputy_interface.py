from abc import ABC, abstractmethod
from typing import List

from domain.entities import DeputyEntity


class DataDeputyInterface(ABC):
    @abstractmethod
    def save_many(self, deputies: List[DeputyEntity]):
        pass

    @abstractmethod
    def find_all(self) -> List[DeputyEntity]:
        pass

    @abstractmethod
    def find(self, deputy_id: str) -> DeputyEntity:
        pass
