from domain.entities import DeputyEntity


def memory_map_deputy_dict_to_entity(deputy: dict) -> DeputyEntity:
    return DeputyEntity(
        id=deputy["id"],
        name=deputy["name"],
        party=deputy["party"],
    )
