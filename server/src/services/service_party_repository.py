from typing import List

import dados_abertos
from data.party import MemoryDataPartyInterface
from data.voting import MemoryDataVotingInterface
from domain.entities import PartyEntity, PartyOrientationEntity, PeriodEntity
from domain.repositories import PartyRepository


class ServicePartyRepository(PartyRepository):
    def __init__(self):
        self.__data_party_interface__ = MemoryDataPartyInterface()
        self.__data_voting_interface__ = MemoryDataVotingInterface()

    def list(self) -> List[PartyEntity]:
        return self.__data_party_interface__.find_all()

    def seed(self) -> List[PartyEntity]:
        response = dados_abertos.fetch_parties()

        return list(
            map(
                self._map_party_response_to_entity,
                response.json()["dados"],
            )
        )

    def create_many(self, parties: List[PartyEntity]):
        self.__data_party_interface__.save_many(parties)

    def find(self, party_id: str) -> PartyEntity:
        return self.__data_party_interface__.find(party_id)

    def list_orientations(
        self,
        party: PartyEntity,
        period: PeriodEntity = PeriodEntity(),
        keyword: str = "",
    ) -> List[PartyOrientationEntity]:
        return self.__data_voting_interface__.find_party_orientations(
            party=party,
            period=period,
            keyword=keyword,
        )

    def _map_party_response_to_entity(self, party_response: dict) -> PartyEntity:
        return PartyEntity(
            id=str(party_response["id"]),
            name=party_response["nome"],
            initials=party_response["sigla"],
        )
