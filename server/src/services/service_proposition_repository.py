import logging
from typing import List, Union

import dados_abertos
from data.proposition import MemoryDataPropositionRepository
from domain.entities import PropositionDetailsEntity, PropositionEntity
from domain.repositories import PropositionRepository
from mappers import MapPropositionDetailsXMLToEntities, MapPropositionsXMLtoEntities

logger = logging.getLogger(__name__)


class ServicePropositionRepository(PropositionRepository):
    def __init__(self):
        self.__data_proposition_interface__ = MemoryDataPropositionRepository()

    def seed(self, year: int) -> List[PropositionEntity]:
        response = dados_abertos.fetch_propositions(year)

        return MapPropositionsXMLtoEntities(response.text).map()

    def save_propositions(self, propositions: List[PropositionEntity]):
        self.__data_proposition_interface__.save_propositions(propositions)

    def list(self) -> List[PropositionEntity]:
        return self.__data_proposition_interface__.find_all()

    def seed_details(self, proposition: PropositionEntity) -> Union[PropositionDetailsEntity, None]:
        response = dados_abertos.fetch_proposition_details(
            type=proposition.type.value,
            number=proposition.number,
            year=proposition.year,
        )

        if response.status_code == 200:
            return MapPropositionDetailsXMLToEntities(response.text, proposition).map()

        logging.info(f"{proposition.type.value} {proposition.number}/{proposition.year} details seed failed")

        return None

    def save_details(self, proposition: PropositionEntity, proposition_details: PropositionDetailsEntity):
        self.__data_proposition_interface__.save_details(proposition, proposition_details)

    def find_details(self, proposition: PropositionEntity) -> PropositionDetailsEntity:
        return self.__data_proposition_interface__.find_details(proposition)
