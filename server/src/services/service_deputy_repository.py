from typing import List

import dados_abertos
from data.deputy import MemoryDataDeputyInterface
from data.voting import MemoryDataVotingInterface
from domain.entities import DeputyEntity, DeputyVoteEntity, PeriodEntity
from domain.repositories import DeputyRepository
from mappers import MapDeputiesXMLToEntities


class ServiceDeputyRepository(DeputyRepository):
    def __init__(self):
        self.__data_deputy_interface__ = MemoryDataDeputyInterface()
        self.__data_voting_interface__ = MemoryDataVotingInterface()

    def seed(self) -> List[DeputyEntity]:
        response = dados_abertos.fetch_deputies()

        return MapDeputiesXMLToEntities(response.text).map()

    def create_many(self, deputies: List[DeputyEntity]):
        self.__data_deputy_interface__.save_many(deputies)

    def list(self) -> List[DeputyEntity]:
        return self.__data_deputy_interface__.find_all()

    def list_votes(self, deputy: DeputyEntity, period: PeriodEntity, keyword: str = "") -> List[DeputyVoteEntity]:
        return self.__data_voting_interface__.find_deputy_votes(
            deputy=deputy,
            period=period,
            keyword=keyword,
        )

    def find(self, deputy_id: str) -> DeputyEntity:
        return self.__data_deputy_interface__.find(deputy_id)
