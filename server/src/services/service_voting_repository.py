from typing import List, Tuple

import requests
from data.voting import MemoryDataVotingInterface
from domain.entities import DeputyVoteEntity, PartyOrientationEntity, PropositionEntity, VotingEntity
from domain.repositories import VotingRepository
from mappers import MapVotingsXMLToEntities


class ServiceVotingRepository(VotingRepository):
    def __init__(self):
        self.__data_voting_interface__ = MemoryDataVotingInterface()

    def seed_votings(
        self,
        proposition: PropositionEntity,
    ) -> List[Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]]]:
        URL = (
            "https://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx/ObterVotacaoProposicao?"
            + f"tipo={proposition.type.value}&numero={proposition.number}&ano={proposition.year}"
        )
        response = requests.get(URL)

        return MapVotingsXMLToEntities(response.text, proposition).map()

    def save_votings(
        self,
        proposition: PropositionEntity,
        votings: List[Tuple[VotingEntity, List[PartyOrientationEntity], List[DeputyVoteEntity]]],
    ):
        self.__data_voting_interface__.save_many(proposition, votings)
