import requests


def fetch_parties():
    URL = "https://dadosabertos.camara.leg.br/api/v2/partidos?itens=100&ordem=ASC&ordenarPor=sigla"

    return requests.get(URL)
