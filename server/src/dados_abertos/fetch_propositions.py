import requests


def fetch_propositions(year: int):
    URL = (
        "https://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx/"
        + "ListarProposicoesVotadasEmPlenario?"
        + f"ano={year}&tipo="
    )

    return requests.get(URL)
