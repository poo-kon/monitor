import requests


def fetch_proposition_details(type: str, number: int, year: int):
    URL = (
        "https://www.camara.leg.br/SitCamaraWS/Proposicoes.asmx/"
        + "ObterProposicao?"
        + f"tipo={type}&numero={number}&ano={year}"
    )

    return requests.get(URL)
