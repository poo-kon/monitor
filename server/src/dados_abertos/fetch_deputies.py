import requests


def fetch_deputies():
    URL = "https://www.camara.leg.br/SitCamaraWS/Deputados.asmx/ObterDeputados"

    return requests.get(URL)
