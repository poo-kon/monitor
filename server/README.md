# **Local Installation**

In order to run the server locally, you need to install and configure the following dependencies:
- [poetry](https://python-poetry.org/docs/#installation): package manager
- [pyenv](https://github.com/pyenv/pyenv): allows multiple Python versions installations

After configuring the dependencies above, run `poetry shell` to start a virtual shell that runs a Python version that matches this project Python version.

Then, run `poetry install` to download this project dependencies.

## **Linting**

Run `black .` to run lint in all files.

## **Tests**

Test files in this project are placed in the [tests](src/tests) folder.\
In order to run the tests, first you need to start the virtual shell by running `poetry shell`.\
Then, run `pytest` to execute all tests.

## **Seed**

Run script ``seed.sh`` to fetch data about deputies, parties, propositions and votes.\
The data will be saved as JSON
in the folder *src/memory_data*.\
The fetch may take 60-120 seconds.